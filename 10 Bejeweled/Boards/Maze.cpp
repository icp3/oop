//
// Created by ian on 2/7/19.
//

#include "Maze.h"
#include <cstdlib>
#include <tuple>


Maze::Maze(unsigned long width, unsigned long height, unsigned int Type)
{
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CONSTRUCTOR + "TypeNum Set");

    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CONSTRUCTOR + "Random Number Generator started");

    debuggy( "Horizontal: " + std::to_string(width));

    debuggy( "Vertical: " + std::to_string(height));

    for(int x = 0; x < width; ++x) {
        maze.emplace_back();
        for (int y = 0; y < height; ++y) {
            maze[x].emplace_back(new Standard());
        }
    }


    if(DEBUG::debug){
        for(int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y ){
                std::cout << maze[x][y]->get_Color().first;
            }
            std::cout << "\n";
        }
    }

    debuggy(MAZE_DEBUG::Maze_Debug + "Finished");
}


//Maze::Maze(const std::vector<std::vector<int>> &maze) : maze(maze), column(maze.size()),row(maze[0].size())  {}

Maze::~Maze() {
    for(auto y: maze)
        y.clear();
    maze.clear();
}




