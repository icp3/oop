//
// Created by ian on 2/7/19.
//

#ifndef INC_10_BEJEWELED_MAZE_H
#define INC_10_BEJEWELED_MAZE_H
#pragma once

#include "MZ.h"
#include "../Types/Types Overload.h"
#include "settings.h"
#include <vector>
#include <deque>
#include <random>
#include <SFML/Graphics/CircleShape.hpp>


using std::vector;

namespace MAZE_DEBUG{
    static const std::string Maze_Debug = "Maze: ";
    static const std::string CONSTRUCTOR = "constructor: ";
    static const std::string CHECK_EVERYWHERE = "CHECK_EVERYWHERE: ";
    static const std::string CHECK_LOCATION = "CHECK_LOCATION: ";
    static const std::string GET_MAZE = "GET_MAZE: ";

    static const std::string CHECK_LOCATION_SINGLE = "CHECK_LOCATION_SINGLE: ";
    static const std::string CHECK_VERTICAL = "CHECK_VERTICAL: ";
    static const std::string CHECK_HORIZONTAL = "CHECK_HORIZONTAL: ";
    static const std::string CHECK_LINE_FUNCTION_HORIZONTAL = "CHECK_LINE_FUNCTION_HORIZONTAL: ";
    static const std::string CHECK_LINE_FUNCTION_VERTICAL = "CHECK_LINE_FUNCTION_VERTICAL: ";

    static const std::string CHECK_BOMB_HORIZONTAL = "CHECK_BOMB_HORIZONTAL: ";
    static const std::string CHECK_BOMB_VERTICAL = "CHECK_BOMB_VERTICAL: ";
    static const std::string CHECK_BOMB_LOCATION = "CHECK_BOMB_LOCATION: ";
    static const std::string REPLACE_NUKE_HORIZONTAL = "REPLACE_NUKE_HORIZONTAL: ";
    static const std::string REPLACE_NUKE_VERTICAL = "REPLACE_NUKE_VERTICAL: ";

    static const std::string REPLACE_BOMB_HORIZONTAL = "REPLACE_NUKE_HORIZONTAL: ";
    static const std::string REPLACE_BOMB_VERTICAL = "REPLACE_NUKE_VERTICAL: ";
    static const std::string REPLACE_LINE_HORIZONTAL = "REPLACE_NUKE_HORIZONTAL: ";
    static const std::string REPLACE_LINE_VERTICAL = "REPLACE_NUKE_VERTICAL: ";
    static const std::string REPLACE_STANDARD_HORIZONTAL = "REPLACE_NUKE_HORIZONTAL: ";

    static const std::string REPLACE_STANDARD_VERTICAL = "REPLACE_NUKE_VERTICAL: ";
    static const std::string REPLACE_SWIRL_HORIZONTAL = "REPLACE_NUKE_HORIZONTAL: ";
    static const std::string REPLACE_SWIRL_VERTICAL = "REPLACE_NUKE_VERTICAL: ";
    static const std::string REPLACE_ITEM_VERTICAL = "REPLACE_ITEM_VERTICAL: ";
    static const std::string REPLACE_ITEM_HORIZONTAL = "REPLACE_ITEM_HORIZONTAL: ";

    static const std::string BOMB_EXPLOSION_HORIZONTAL = "BOMB_EXPLOSION_HORIZONTAL: ";
    static const std::string BOMB_EXPLOSION_VERTICAL = "BOMB_EXPLOSION_VERTICAL: ";
    static const std::string NUKE_EXPLOSION_HORIZONTAL = "NUKE_EXPLOSION_HORIZONTAL: ";
    static const std::string NUKE_EXPLOSION_VERTICAL = "NUKE_EXPLOSION_VERTICAL: ";
    static const std::string SWIRL_EXPLOSION_HORIZONTAL = "SWIRL_EXPLOSION_HORIZONTAL: ";

    static const std::string SWIRL_EXPLOSION_VERTICAL = "SWIRL_EXPLOSION_VERTICAL: ";
    static const std::string LINE_EXPLOSION_HORIZONTAL = "LINE_EXPLOSION_HORIZONTAL: ";
    static const std::string LINE_EXPLOSION_VERTICAL = "LINE_EXPLOSION_VERTICAL: ";
    static const std::string STANDARD_EXPLOSION = "LINE_EXPLOSION_HORIZONTAL: ";
    static const std::string REPLACE_STRAIGHT_LINE_HORIZONTAL = "REPLACE_STRAIGHT_LINE_HORIZONTAL: ";
    static const std::string REPLACE_STRAIGHT_LINE_VERTICAL = "REPLACE_STRAIGHT_LINE_VERTICAL: ";

}

class Maze{
public:
    Maze(){};
    /** Constructors **/
    Maze(unsigned long WidthAndLength, unsigned int TypeNum) : Maze( WidthAndLength, WidthAndLength, TypeNum) {};

    Maze( unsigned long Width, unsigned long Length, unsigned int TypeNum);

    ~Maze();

    /** Getters **/

    unsigned long getColumn() const{return maze.size();};

    unsigned long getRow() const{return maze[0].size();};

    bool check_everywhere();

    bool check_location(const std::pair<int, int>& target_color,const std::pair<int, int>& target_location);

    void get_maze(vector<vector<sf::Color>>& grid);

protected:

    vector<vector<Types*>> maze;

    bool check_location_single(const std::pair<int, int>& Location);

    bool check_vertical(const std::pair<int,int>& location,
        const std::pair<int,TYPES::COLOR>& vertical,
        bool vertical_bomb);
    bool check_horizontal(const std::pair<int,int>& location,
        const std::pair<int,TYPES::COLOR>& horizontal,
        bool horizontal_bomb);

    std::pair<int,TYPES::COLOR> check_line_function_horizontal(const std::pair<int,int>& location);
    std::pair<int,TYPES::COLOR> check_line_function_vertical(const std::pair<int,int>& location);

    bool check_bomb_horizontal(const std::pair<int,int>& location,
        const std::pair<int,TYPES::COLOR>& horizontal);
    bool check_bomb_vertical(const std::pair<int,int>& location,
        const std::pair<int,TYPES::COLOR>& vertical);

    bool check_bomb_location(const std::pair<int,int>& locaiton,
        TYPES::COLOR Colour);

    bool replace_nuke_horizontal(const std::pair<int,int>& location,
        TYPES::COLOR Colour);
    bool replace_nuke_vertical(const std::pair<int,int>& location,
        TYPES::COLOR Colour);

    bool replace_swirl_horizontal(const std::pair<int,int>& location,
        int count, TYPES::COLOR Colour);
    bool replace_swirl_vertical(const std::pair<int,int>& location,
        int count, TYPES::COLOR Colour);

    bool replace_bomb_horizontal(const std::pair<int,int>& location,
        TYPES::COLOR Colour);
    bool replace_bomb_vertical(const std::pair<int,int>& location,
        TYPES::COLOR Colour);

    bool replace_line_horizontal(const std::pair<int,int>& location,
        TYPES::COLOR Colour);
    bool replace_line_vertical(const std::pair<int,int>& location,
        TYPES::COLOR Colour);

    bool replace_standard_horizontal(const std::pair<int,int>& location,
        TYPES::COLOR Colour);
    bool replace_standard_vertical(const std::pair<int,int>& location,
        TYPES::COLOR Colour);

    void replace_straight_line_horizontal(const std::pair<int,int>& location,
        TYPES::COLOR Colour, int count, Types* type);
    void replace_straight_line_vertical(const std::pair<int,int>& location,
        TYPES::COLOR Colour, int count, Types* type);

    void replace_item_horizontal(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type = new Standard());
    void replace_item_vertical(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type = new Standard());

    void swirl_explosion_horizontal(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);
    void swirl_explosion_vertical(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);

    void bomb_explosion_horizontal(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);
    void bomb_explosion_vertical(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);

    void nuke_explosion_horizontal(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);
    void nuke_explosion_vertical(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);

    void line_explosion_horizontal(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);
    void line_explosion_vertical(const std::pair<int,int>& location, TYPES::COLOR Colour, Types* type);

    void standard_explosion(const std::pair<int,int>& location, Types* type = new Standard());
};


#endif //INC_10_BEJEWELED_MAZE_H
