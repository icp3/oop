//
// Created by ian on 5/2/19.
//

#include "Maze.h"

void Maze::replace_item_vertical(const std::pair<int, int> &location, TYPES::COLOR Colour, Types* type) {
    Types* temp = new Standard();
    switch(maze[location.first][location.second]->get_Action()){
        case TYPES::Standard:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_VERTICAL + "Standard");
            standard_explosion(location,type);
            break;
        case TYPES::BOMB:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_VERTICAL + "Bomb");
            bomb_explosion_vertical(location,Colour,type);
            break;
        case TYPES::LINE:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_VERTICAL + "Line");
            line_explosion_vertical(location,Colour,type);
            break;
        case TYPES::SWIRL:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_VERTICAL + "Swirl");
            swirl_explosion_vertical(location,Colour,type);
            break;
        case TYPES::NUKE:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_VERTICAL + "Nuke");
            nuke_explosion_vertical(location,Colour,type);
            break;
    }
//    delete temp;
}

void Maze::replace_item_horizontal(const std::pair<int, int> &location, TYPES::COLOR Colour, Types* type) {
    switch(maze[location.first][location.second]->get_Action()){
        case TYPES::Standard:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_HORIZONTAL + "Standard");
            standard_explosion(location, type);
            break;
        case TYPES::BOMB:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_HORIZONTAL + "Bomb");
            bomb_explosion_horizontal(location,Colour,type);
            break;
        case TYPES::LINE:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_HORIZONTAL + "Line");
            line_explosion_horizontal(location,Colour,type);
            break;
        case TYPES::SWIRL:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_HORIZONTAL + "Swirl");
            swirl_explosion_horizontal(location,Colour,type);
            break;
        case TYPES::NUKE:
            debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_ITEM_HORIZONTAL + "Nuke");
            nuke_explosion_horizontal(location,Colour,type);
            break;
    }
//    delete temp;
}