//
// Created by ian on 2/28/19.
//

#ifndef INC_10_BEJEWELED_SETTINGS_NAMESPACE_H
#define INC_10_BEJEWELED_SETTINGS_NAMESPACE_H


namespace Settings_Namespace {
    enum Setup{
        TIME                = 1,
        IN_PLACE_CREATE     = 1 << 1,
        NO_FILL             = 1 << 2,
    };
};


#endif //INC_10_BEJEWELED_SETTINGS_NAMESPACE_H
