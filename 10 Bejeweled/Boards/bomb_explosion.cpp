//
// Created by ian on 5/2/19.
//


#include "Maze.h"

void Maze::bomb_explosion_horizontal(const std::pair<int, int> &location, TYPES::COLOR Colour, Types *type) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::BOMB_EXPLOSION_HORIZONTAL + "Started");
    int left_start = location.first, right_end = location.first,
        down_start = location.second, up_end = location.second;
    for(int x = 0; x < 2 && left_start > 0; ++x)
        --left_start;
    for(int x = 0; x < 2 && down_start > 0; ++x)
        --down_start;
    for(int x = 0; x < 2 && right_end < maze.size(); ++x)
        ++right_end;
    for(int x = 0; x < 2 && up_end < maze[location.first].size(); ++x)
        ++up_end;
    int temp;
    while(left_start < right_end) {
        temp = up_end;
        while (temp > down_start) {
            replace_item_horizontal(std::pair<int,int>(left_start, temp),Colour);
            --temp;
        }
        ++left_start;
    }
}

void Maze::bomb_explosion_vertical(const std::pair<int, int> &location, TYPES::COLOR Colour, Types *type) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::BOMB_EXPLOSION_VERTICAL + "Started");
    int left_start = location.first, right_end = location.first,
            down_start = location.second, up_end = location.second;
    for(int x = 0; x < 2 && left_start > 0; ++x)
        --left_start;
    for(int x = 0; x < 2 && down_start > 0; ++x)
        --down_start;
    for(int x = 0; x < 2 && right_end < maze.size(); ++x)
        ++right_end;
    for(int x = 0; x < 2 && up_end < maze[location.first].size(); ++x)
        ++up_end;
    int temp;
    while(left_start < right_end) {
        temp = up_end;
        while (temp > down_start) {
            replace_item_vertical(std::pair<int,int>(left_start, temp),Colour);
            --temp;
        }
        ++left_start;
    }
}
