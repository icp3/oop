//
// Created by ian on 4/11/19.
//

#include "Maze.h"

std::pair<int,TYPES::COLOR> Maze::check_line_function_vertical(const std::pair<int, int> &location) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LINE_FUNCTION_VERTICAL + "Started");
    auto Colours = maze[location.first][location.second]->get_Color();

    int boundary = 0;
    while(location.second + boundary < maze[location.first].size() && boundary < 5)
        ++boundary;

    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LINE_FUNCTION_VERTICAL + "Boundary Found: " + std::to_string(boundary));

    for(int y = 0; Colours.second && y < boundary; ++y)
        Colours = maze[location.first][location.second + y]->get_Color();

    int final = 0;
    while( final < boundary &&
        Colours.first == maze[location.first][location.second + final]->get_Color().first)
            ++final;

    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LINE_FUNCTION_VERTICAL + "Returning: count: " + std::to_string(final) + "Color: " + Colours.first);

    return std::pair<int,TYPES::COLOR>(final, Colours.first);
}

std::pair<int,TYPES::COLOR> Maze::check_line_function_horizontal(const std::pair<int, int> &location) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LINE_FUNCTION_HORIZONTAL + "Started: x: " + std::to_string(location.first) + " y: " + std::to_string(location.second));
    auto Colours = maze[location.first][location.second];

    int boundary = 0;
    while(location.first + boundary < maze.size() && boundary < 5)
        ++boundary;
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LINE_FUNCTION_HORIZONTAL + "Boundary Found: " + std::to_string(boundary));


    for(int x = 0; Colours->get_Color().second && x < boundary; ++x)
        Colours = maze[location.first+x][location.second];

    int final = 0;
    while( final < boundary && Colours == maze[location.first + final][location.second])
        ++final;

    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LINE_FUNCTION_HORIZONTAL + "Returning: count: " + std::to_string(final) + " Color: " + Colours->get_Color().first);

    return std::pair<int,TYPES::COLOR>(final, Colours->get_Color().first);
}
