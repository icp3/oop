//
// Created by ian on 4/16/19.
//

#include "Maze.h"

bool Maze::check_bomb_horizontal(const std::pair<int,int>& location,
                           const std::pair<int,TYPES::COLOR>& horizontal){
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_BOMB_HORIZONTAL + "Started");
    bool up = false, down = false;
    if(location.second + 1 < maze[location.first].size())
        up = true;
    if(location.second - 1 >= 0)
        down = true;

    for(int x = 0; x < horizontal.first - 1; ++x){
        if(up)
            if(check_bomb_location(std::pair<int,int>(location.first + x,location.second), horizontal.second))
                return true;
        if(down)
            if(check_bomb_location(std::pair<int,int>(location.first + x,location.second - 1), horizontal.second))
                return true;
    }
    return false;
}

bool Maze::check_bomb_vertical(const std::pair<int,int>& location,
                                 const std::pair<int,TYPES::COLOR>& vertical){
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_BOMB_VERTICAL + "Started");
    bool left = false, right = false;
    if(location.first + 1 < maze.size())
        right = true;
    if(location.first - 1 >= 0)
        left = true;

    for(int x = 0; x < vertical.first - 1; ++x){
        if(left)
            if(check_bomb_location(std::pair<int,int>(location.first-1,location.second+x), vertical.second))
                return true;
        if(right)
            if(check_bomb_location(std::pair<int,int>(location.first,location.second+x), vertical.second))
                return true;
    }
    return false;
}

bool Maze::check_bomb_location(const std::pair<int, int> &location,
                               TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_BOMB_LOCATION + "Started");
    if(location.first + 1 >= maze.size())
        return false;
    if(location.second + 1 >= maze[location.first].size())
        return false;
    return (maze[location.first][location.second]->get_Color().first ==
        maze[location.first+1][location.second]->get_Color().first &&
        maze[location.first][location.second]->get_Color().first ==
        maze[location.first+1][location.second+1]->get_Color().first &&
        maze[location.first][location.first +1]->get_Color().first);
}