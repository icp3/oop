//
// Created by ian on 4/23/19.
//


#include "Maze.h"

bool Maze::check_everywhere() {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_EVERYWHERE + "Started");
    bool did_you_destroy_something = false;
    for (int x = 0; x < maze.size(); ++x) {
        for (int y = 0; y < maze[x].size(); ++y) {
            if(check_location_single(std::pair<int, int>(x, y))) {
                did_you_destroy_something = true;
            }
        }
    }
    return did_you_destroy_something;
}


