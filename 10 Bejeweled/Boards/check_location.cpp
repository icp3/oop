//
// Created by ian on 4/9/19.
//
#include "Maze.h"
bool Maze::check_location_single(const std::pair<int,int>& location) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LOCATION_SINGLE + "x: " + std::to_string(location.first) + " y: " + std::to_string(location.second));
    bool horizontal_bomb = false, vertical_bomb = false;
    auto horizontal = check_line_function_horizontal(location),
         vertical = check_line_function_vertical(location);

    if(horizontal.first > 1 && horizontal.first < 5)
        horizontal_bomb = check_bomb_horizontal(location, horizontal);
    if(vertical.first > 1 && vertical.first < 5)
        vertical_bomb = check_bomb_vertical(location, vertical);

    return (check_horizontal(location, horizontal, horizontal_bomb) ||
            check_vertical(location, vertical, vertical_bomb));
}

bool Maze::check_horizontal(const std::pair<int,int>& location,
        const std::pair<int,TYPES::COLOR>& horizontal,
        bool horizontal_bomb) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_HORIZONTAL + "Started");

    switch (horizontal.first) {
        case 5:
            return replace_nuke_horizontal(location, horizontal.second);
        case 4:
            if (horizontal_bomb)
                return replace_swirl_horizontal(location, 4, horizontal.second);
            else
                return replace_line_horizontal(location, horizontal.second);
        case 3:
            if (horizontal_bomb)
                return replace_swirl_horizontal(location, 3, horizontal.second);
            else
                return replace_standard_horizontal(location, horizontal.second);
        case 2:
            if (horizontal_bomb)
                return replace_bomb_horizontal(location, horizontal.second);
        default:
            return false;
    }
}

bool Maze::check_vertical(const std::pair<int,int>& location,
        const std::pair<int,TYPES::COLOR>& vertical,
        bool vertical_bomb) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_VERTICAL + "Started");

    switch (vertical.first) {
        case 5:
//            return replace_nuke_vertical(location, vertical.second);
        case 4:
//            if (vertical_bomb)
//                return replace_swirl_vertical(location, 4, vertical.second);
//            else
                return replace_line_vertical(location, vertical.second);
        case 3:
//            if (vertical_bomb)
//                return replace_swirl_vertical(location, 3, vertical.second);
//            else
                return replace_standard_vertical(location, vertical.second);
        case 2:
            if (vertical_bomb)
                return replace_bomb_vertical(location, vertical.second);
        default:
            return false;
    }
}

bool Maze::check_location(const std::pair<int, int>& Target_Color, const std::pair<int, int>& target_location) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LOCATION + "Started");
    //    std::swap(maze[Target_Color.first][Target_Color.second], maze[target_location.first][target_location.second]);
    Types* swapper = maze[Target_Color.first][Target_Color.second];
    maze[Target_Color.first][Target_Color.second] = maze[target_location.first][target_location.second];
    maze[target_location.first][target_location.second]= swapper;
    debuggy(MAZE_DEBUG::Maze_Debug +MAZE_DEBUG::CHECK_LOCATION + "Swapped");

    bool check = false;

    while(check_everywhere()) {
        debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::CHECK_LOCATION + "Found a Match");
        check = true;
    }

    if(check)
        return true;

//    std::swap(maze[Target_Color.first][Target_Color.second], maze[target_location.first][target_location.second]);
    swapper = maze[Target_Color.first][Target_Color.second];
    maze[Target_Color.first][Target_Color.second] = maze[target_location.first][target_location.second];
    maze[target_location.first][target_location.second]= swapper;
    debuggy( MAZE_DEBUG::Maze_Debug + "Check_location: false" );
    return false;
}
