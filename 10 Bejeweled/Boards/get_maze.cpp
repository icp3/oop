//
// Created by ian on 4/30/19.
//

#include "Maze.h"
#include "../gui/gui.h"


void Maze::get_maze(vector<vector<sf::Color>>& grid) {
    for(int x= 0; x < maze.size(); ++x)
        for( int y = 0; y < maze.size(); ++y)
            grid[x][y] = setcolor(maze[x][y]->get_Color().first);
}

