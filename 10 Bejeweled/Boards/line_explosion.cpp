//
// Created by ian on 5/2/19.
//

#include "Maze.h"

void Maze::line_explosion_horizontal(const std::pair<int, int> &location, TYPES::COLOR Colour, Types *type) {
    for(int x = 0; x < maze.size(); ++x)
        replace_item_horizontal(std::pair<int,int>(x,location.second),Colour);
}

void Maze::line_explosion_vertical(const std::pair<int, int> &location, TYPES::COLOR Colour, Types *type) {
    for(int y = maze[location.first].size()-1; y >=0; --y)
        replace_item_horizontal(std::pair<int,int>(location.first,y),Colour);
}