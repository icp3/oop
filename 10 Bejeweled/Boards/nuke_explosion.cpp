//
// Created by ian on 5/2/19.
//


#include "Maze.h"

void Maze::nuke_explosion_horizontal(const std::pair<int, int> &location, TYPES::COLOR Colour, Types *type) {

    for(int y = maze[0].size() - 1; y >= 0; --y)
        for(int x = 0; x < maze.size(); ++x)
            replace_bomb_horizontal(std::pair<int,int>(x,y),Colour);

}

void Maze::nuke_explosion_vertical(const std::pair<int, int> &location, TYPES::COLOR Colour, Types *type) {
    for(int y = maze[0].size() - 1; y >= 0; --y)
        for(int x = 0; x < maze.size(); ++x)
            replace_bomb_vertical(std::pair<int,int>(x,y),Colour);
}

