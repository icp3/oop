//
// Created by ian on 5/2/19.
//

#include "Maze.h"

bool Maze::replace_bomb_horizontal(const std::pair<int, int>& location,
                                   TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_BOMB_HORIZONTAL + "Started");
    bool up = (location.second + 1 < maze[location.first].size()),
        down = (location.second - 1 >= 0);

    if(up) {
        if (maze[location.first][location.second]->get_Color().first ==
            maze[location.first + 1][location.second]->get_Color().first &&
            maze[location.first][location.second]->get_Color().first ==
            maze[location.first + 1][location.second + 1]->get_Color().first &&
            maze[location.first][location.second]->get_Color().first ==
            maze[location.first][location.second + 1]->get_Color().first) {
            replace_item_horizontal(std::pair<int, int>(location.first, location.second + 1), Colour);
            replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
            replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second), Colour);
            replace_item_horizontal(std::pair<int, int>(location.first, location.second), Colour, new Bomb());
        }
    }
    if( down)
        if(maze[location.first][location.second]->get_Color().first ==
           maze[location.first-1][location.second]->get_Color().first &&
           maze[location.first][location.second]->get_Color().first ==
           maze[location.first+1][location.second-1]->get_Color().first &&
           maze[location.first][location.second]->get_Color().first ==
           maze[location.first][location.second-1]->get_Color().first){
            replace_item_horizontal(std::pair<int,int>(location.first,location.second + 1),Colour);
            replace_item_horizontal(std::pair<int,int>(location.first - 1,location.second + 1),Colour);
            replace_item_horizontal(std::pair<int,int>(location.first - 1,location.second),Colour);
            replace_item_horizontal(std::pair<int,int>(location.first,location.second),Colour, new Bomb());
        }
    return true;
}

bool Maze::replace_bomb_vertical(const std::pair<int, int>& location,
                                 TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_BOMB_VERTICAL + "Started");

    bool right = (location.first + 1 < maze[location.first].size()),
         left = (location.first - 1 >= 0);

    if(right) {
        if (maze[location.first][location.second]->get_Color().first ==
            maze[location.first + 1][location.second]->get_Color().first &&
            maze[location.first][location.second]->get_Color().first ==
            maze[location.first + 1][location.second + 1]->get_Color().first &&
            maze[location.first][location.second]->get_Color().first ==
            maze[location.first][location.second + 1]->get_Color().first) {
            replace_item_vertical(std::pair<int, int>(location.first, location.second + 1), Colour);
            replace_item_vertical(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
            replace_item_vertical(std::pair<int, int>(location.first + 1, location.second), Colour);
            replace_item_vertical(std::pair<int, int>(location.first, location.second), Colour, new Bomb());
        }
    }
    if( left)
        if(maze[location.first][location.second]->get_Color().first ==
           maze[location.first-1][location.second]->get_Color().first &&
           maze[location.first][location.second]->get_Color().first ==
           maze[location.first-1][location.second+1]->get_Color().first &&
           maze[location.first][location.second]->get_Color().first ==
           maze[location.first][location.second+1]->get_Color().first){
            replace_item_vertical(std::pair<int,int>(location.first,location.second + 1),Colour);
            replace_item_vertical(std::pair<int,int>(location.first - 1,location.second + 1),Colour);
            replace_item_vertical(std::pair<int,int>(location.first - 1,location.second),Colour);
            replace_item_vertical(std::pair<int,int>(location.first,location.second),Colour, new Bomb());
        }
    return true;
}

