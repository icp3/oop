//
// Created by ian on 5/2/19.
//

#include "Maze.h"

bool Maze::replace_line_horizontal(const std::pair<int, int>& location,
                                   TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_LINE_HORIZONTAL + "Started");

    replace_straight_line_horizontal(location, Colour, 4, new Line());
    return false;
}

bool Maze::replace_line_vertical(const std::pair<int, int>& location,
                                 TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_LINE_VERTICAL + "Started");
    replace_straight_line_vertical(location, Colour, 4, new Line());
    return false;
}