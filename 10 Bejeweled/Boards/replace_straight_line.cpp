//
// Created by ian on 5/2/19.
//

#include "Maze.h"

void Maze::replace_straight_line_horizontal(const std::pair<int, int> &location,
        TYPES::COLOR Colour, int count, Types *type) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_STRAIGHT_LINE_HORIZONTAL + "Started");
    for(int x = 1; x < count; ++x){
        replace_item_horizontal(std::pair<int,int>(location.first + count - x, location.second), Colour);
    }
    replace_item_horizontal(location,Colour,type);
}

void Maze::replace_straight_line_vertical(const std::pair<int, int> &location,
        TYPES::COLOR Colour, int count, Types *type) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_STRAIGHT_LINE_VERTICAL + "Started");
    for(int x = 1; x < count; ++x){
        replace_item_vertical(std::pair<int,int>(location.first, location.second + count - x), Colour);
    }
    replace_item_horizontal(location,Colour,type);
}