//
// Created by ian on 5/2/19.
//

#include "Maze.h"


bool Maze::replace_swirl_horizontal(const std::pair<int, int>& location, int count, TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_SWIRL_HORIZONTAL + "Started");
    bool up = (location.second + 1 < maze[location.first].size()),
        down = location.second - 1 >= 0;
    int starting_point = 0;
    switch(count) {
        case 3:
            if (up) {
                if (maze[location.first + 1][location.second + 1]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    if (maze[location.first][location.second + 1]->get_Color().first ==
                        maze[location.first][location.second]->get_Color().first) {
                        replace_item_horizontal(std::pair<int, int>(location.first, location.second + 1), Colour);
                        replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    }
                    if (maze[location.first + 2][location.second + 1]->get_Color().first ==
                        maze[location.first][location.second]->get_Color().first) {
                        replace_item_horizontal(std::pair<int, int>(location.first, location.second + 1), Colour);
                        replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    }
                }
            }
            if (down) {
                if (maze[location.first + 1][location.second - 1]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    if (maze[location.first][location.second - 1]->get_Color().first ==
                        maze[location.first][location.second]->get_Color().first) {
                        replace_item_horizontal(std::pair<int, int>(location.first, location.second + 1), Colour);
                        replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    }
                    if (maze[location.first + 2][location.second - 1]->get_Color().first ==
                        maze[location.first][location.second]->get_Color().first) {
                        replace_item_horizontal(std::pair<int, int>(location.first, location.second + 1), Colour);
                        replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    }
                }
            }
            break;
        case 4:
            if (up) {
                if (maze[location.first + 1][location.second + 1]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first &&
                    maze[location.first + 2][location.second + 1]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first + 2, location.second + 1), Colour);
                } else if (maze[location.first + 2][location.second + 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first &&
                           maze[location.first + 3][location.second + 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 2, location.second + 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first + 3, location.second + 1), Colour);
                    starting_point = 1;
                } else if (maze[location.first][location.second + 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first &&
                           maze[location.first + 1][location.second + 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first, location.second + 1), Colour);
                }
            }
            if (down) {
                if (maze[location.first + 1][location.second - 1]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first &&
                    maze[location.first + 2][location.second - 1]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second - 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first + 2, location.second - 1), Colour);
                } else if (maze[location.first + 2][location.second - 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first &&
                           maze[location.first + 3][location.second - 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 2, location.second - 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first + 3, location.second - 1), Colour);
                    starting_point = 1;
                } else if (maze[location.first][location.second - 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first &&
                           maze[location.first + 1][location.second - 1]->get_Color().first ==
                           maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second - 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first, location.second - 1), Colour);
                }
            }
            break;
        default:
            throw std::logic_error("Illegal usage, count must be 3 or 4");
    }
    for(int x = 1; x < 3; ++x)
        replace_item_horizontal(std::pair<int,int>(location.first + starting_point + x, location.second),Colour);
    replace_item_horizontal(std::pair<int,int>(location.first + starting_point, location.second),Colour,new Swirl());
    return true;
}

bool Maze::replace_swirl_vertical(const std::pair<int, int>& location, int count, TYPES::COLOR Colour) {
    debuggy(MAZE_DEBUG::Maze_Debug + MAZE_DEBUG::REPLACE_SWIRL_VERTICAL + "Started");

    bool right = (location.first + 1 < maze.size()),
         left = location.first - 1 >= 0;
    int starting_point = 0;
    if(count == 3) {
        if (right) {
            if (maze[location.first + 1][location.second + 1]->get_Color().first ==
                maze[location.first][location.second]->get_Color().first) {
                if (maze[location.first + 1][location.second]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                }
                if (maze[location.first + 1][location.second + 2]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first + 2, location.second + 1), Colour);
                }
            }
        }
        if (left) {
            if (maze[location.first - 1][location.second + 1]->get_Color().first ==
                maze[location.first][location.second]->get_Color().first) {
                if (maze[location.first - 1][location.second]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 1), Colour);
                }
                if (maze[location.first - 1][location.second + 2]->get_Color().first ==
                    maze[location.first][location.second]->get_Color().first) {
                    replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 1), Colour);
                    replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 2), Colour);
                }
            }
        }
    } else if(count == 4) {
        if (right) {
            if (maze[location.first + 1][location.second + 1]->get_Color().first ==
                maze[location.first][location.second]->get_Color().first &&
                maze[location.first + 1][location.second]->get_Color().first ==
                maze[location.first][location.second]->get_Color().first) {
                replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second), Colour);
            } else if (maze[location.first + 1][location.second + 1]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first &&
                       maze[location.first + 1][location.second + 2]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first) {
                replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 1), Colour);
                replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 2), Colour);
            } else if (maze[location.first + 1][location.second + 1]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first &&
                       maze[location.first + 1][location.second + 1]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first) {
                replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 2), Colour);
                replace_item_horizontal(std::pair<int, int>(location.first + 1, location.second + 3), Colour);
                starting_point = 1;
            }
        }
        if (left) {
            if (maze[location.first - 1][location.second + 1]->get_Color().first ==
                maze[location.first][location.second]->get_Color().first &&
                maze[location.first - 1][location.second]->get_Color().first ==
                maze[location.first][location.second]->get_Color().first) {
                replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 1), Colour);
                replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second), Colour);
            } else if (maze[location.first - 1][location.second + 1]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first &&
                       maze[location.first - 1][location.second + 2]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first) {
                replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 1), Colour);
                replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 2), Colour);
            } else if (maze[location.first - 1][location.second + 1]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first &&
                       maze[location.first - 1][location.second + 1]->get_Color().first ==
                       maze[location.first][location.second]->get_Color().first) {
                replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 2), Colour);
                replace_item_horizontal(std::pair<int, int>(location.first - 1, location.second + 3), Colour);
                starting_point = 1;
            }
        }
    }
    for(int x = 1; x < 3; ++x)
        replace_item_horizontal(std::pair<int,int>(location.first , location.second + starting_point + x),Colour);
    replace_item_horizontal(std::pair<int,int>(location.first, location.second + starting_point),Colour,new Swirl());
    return true;
}
