//
// Created by ian on 2/7/19.
//

#include <stdexcept>
#include "settings.h"

Settings::Settings(bool time, bool inPlaceCreate, bool noFill) {
    bool array[3] = {time, inPlaceCreate, noFill};
    for (int i = 0; i < 3; ++i) {
        if (array[i]) {
            data |= static_cast<unsigned int>(array[i]) << i;
        }
    }
    data |= static_cast<unsigned int>(3) << 24;
}


Settings::Settings(bool* settings, unsigned short number) {
    if(number > 24) {
        throw std::logic_error("To many Rules added.");
        return;
    }
    for(int i = 0; i < number; ++i)
    {
        if(settings[i])
        {
            data |= static_cast<unsigned int>(settings[i]) << i;
        }
    }
    data |= static_cast<unsigned int>(number) << 24;
}


bool Settings::get_time_rule() const {
    return(static_cast<bool>(data & Settings_Namespace::TIME));
}

bool Settings::get_no_fill_rule() const {
    return(static_cast<bool>(data & Settings_Namespace::NO_FILL));

}

bool Settings::get_in_place_create() const {
    return(static_cast<bool>(data & Settings_Namespace::IN_PLACE_CREATE));
}

unsigned short Settings::get_number_of_settings() {
    return (static_cast<unsigned short>( data >> 24 ));
}
