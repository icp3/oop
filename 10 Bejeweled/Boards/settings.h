//
// Created by ian on 2/7/19.
//

#ifndef INC_10_BEJEWELED_SETTINGS_H
#define INC_10_BEJEWELED_SETTINGS_H
#pragma once

#include "Settings_Namespace.h"



class Settings{
    unsigned int data;
public:
    Settings() {}
    explicit Settings(bool time = false, bool inPlaceCreate = false, bool noFill = false);

    Settings(bool* settings, unsigned short number);

    bool get_no_fill_rule() const;
    bool get_time_rule() const;
    bool get_in_place_create() const;
    unsigned short get_number_of_settings();

};



#endif //INC_10_BEJEWELED_SETTINGS_H

