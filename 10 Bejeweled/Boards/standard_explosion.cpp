//
// Created by ian on 5/2/19.
//

#include "Maze.h"


void Maze::standard_explosion(const std::pair<int, int> &location, Types *type) {
    for(int y = location.second + 1; y < maze[y].size(); ++y){
        std::swap(maze[location.first][y], maze[location.first][y - 1]);
    }
    std::swap(maze[location.first][maze[location.first].size()-1], type);
}

