//
// Created by ian on 4/18/19.
//

#ifndef INC_10_BEJEWELED_DEBUGGY_H
#define INC_10_BEJEWELED_DEBUGGY_H
#pragma once

#include <iostream>

namespace DEBUG {
    static const std::string Type_Debug = "Type: ";
    static const std::string color_created = "Color Created: ";
    static const std::string Color_Selected = "Color Selected: ";

    static bool debug = true;

}


static void debuggy(const std::string& Message) {
    if(DEBUG::debug){
        std::cout << "Debug: " << Message << "\n";
    }

}
#endif //INC_10_BEJEWELED_DEBUGGY_H
