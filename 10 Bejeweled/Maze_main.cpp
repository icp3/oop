//
// Created by ian on 4/8/19.
//

#include "check.h"
#include "gui/gui.h"

void options(){
    std::cout << "Options: \n\tEasy\n\tMedium\n\tHard\n\n";
}

void game_on(std::string& filename, bool easy, bool medium, bool hard);

int main(int argv, char** argc){
    srand(time(0));
    bool easy = false, medium = false, hard = false;
    std::string filename;

    for(int counter = 1; counter < argv; ++counter){
        std::string str = argc[counter];
        filename = check(str, easy, medium, hard);
        if(!filename.empty())
            break;
    }
    debuggy( "Image: " + filename);

    game_on(filename, easy, medium, hard);

    return 0;
}

void game_on(std::string& filename, bool easy, bool medium, bool hard) {

    GUI* game;

    if (easy) {
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Easy Selected");
        game = new GUI(15, 15);
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Finished Creating Maze");
    } else if (medium) {
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Medium Selected");
        game = new GUI(10, 10);
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Finished Creating Maze");
    } else if (hard) {
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Hard Selected");
        game = new GUI(5, 5);
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Finished Creating Maze");
    } else {
        debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +
                "Error selection");
        options();
        throw std::logic_error("One of above options must be true.");
    }

    while(game->isOpen()){
        game->poll_event();
    }
}