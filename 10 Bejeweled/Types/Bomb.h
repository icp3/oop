//
// Created by ian on 2/28/19.
//

#ifndef INC_10_BEJEWELED_BOMB_H
#define INC_10_BEJEWELED_BOMB_H
#pragma once

#include "types.h"

class Bomb : public Types {
public:
    Bomb(){debuggy(DEBUG::color_created + get_Color().first);};
    virtual std::pair<TYPES::COLOR, bool> get_Color() const{ return( std::pair<TYPES::COLOR, bool>(TYPES::BLACK,true));};
    TYPES::ACTION get_Action() override{ return TYPES::BOMB;};

};


#endif //INC_10_BEJEWELED_BOMB_H
