//
// Created by ian on 2/28/19.
//

#ifndef INC_10_BEJEWELED_LINE_H
#define INC_10_BEJEWELED_LINE_H
#pragma once

#include "types.h"
class Line : public Types{
public:
    Line(){debuggy(DEBUG::Type_Debug + DEBUG::color_created + this->get_Color().first);};
    std::pair<TYPES::COLOR, bool> get_Color() const{return std::pair<TYPES::COLOR, bool>(TYPES::WHITE, true);};
    TYPES::ACTION get_Action(){ return TYPES::LINE;};
};


#endif //INC_10_BEJEWELED_LINE_H
