//
// Created by ian on 2/28/19.
//

#ifndef INC_10_BEJEWELED_NUKE_H
#define INC_10_BEJEWELED_NUKE_H

#include "types.h"

class Nuke : public Types {
private:

public:
    Nuke(){debuggy(DEBUG::Type_Debug + DEBUG::color_created + get_Color().first);};
    virtual std::pair<TYPES::COLOR, bool> get_Color() const { return std::pair<TYPES::COLOR, bool>(TYPES::MAGENTA,true);};
    TYPES::ACTION get_Action(){ return TYPES::NUKE;};
};


#endif //INC_10_BEJEWELED_NUKE_H
