//
// Created by ian on 2/28/19.
//

#ifndef INC_10_BEJEWELED_STANDARD_H
#define INC_10_BEJEWELED_STANDARD_H
#pragma once

#include "types.h"

const int TypeNum = 6;

class Standard : public Types {
    TYPES::COLOR Colour;

public:
    Standard(): Colour(static_cast<TYPES::COLOR >(rand() % TypeNum)) {};

    Standard(TYPES::COLOR colour):
            Colour(colour){ debuggy(DEBUG::Type_Debug + colour );};

    ~Standard(){delete this;};

//    Types::Types(int row1, int col1, int ts):
//            col(col1),row(row1),
//            x(col1 * ts), y(row1 * ts)
//    {
//
//    }
    virtual std::pair<TYPES::COLOR, bool> get_Color() const {return std::pair<TYPES::COLOR, bool>(Colour,false);};
    TYPES::ACTION get_Action() { return TYPES::Standard;};
};




#endif //INC_10_BEJEWELED_STANDARD_H
