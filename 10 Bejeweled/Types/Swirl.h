//
// Created by ian on 3/3/19.
//

#ifndef INC_10_BEJEWELED_SWIRL_H
#define INC_10_BEJEWELED_SWIRL_H
#pragma once


#include "types.h"

class Swirl : public Types {
public:
    virtual std::pair<TYPES::COLOR, bool> get_Color() const { return std::pair<TYPES::COLOR, bool>(TYPES::TRANSPARENT,true);};
    TYPES::ACTION get_Action(){ return TYPES::SWIRL;};
};


#endif //INC_10_BEJEWELED_SWIRL_H
