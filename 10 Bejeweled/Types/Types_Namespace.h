//
// Created by ian on 2/27/19.
//

#ifndef INC_10_BEJEWELED_TYPES_NAMESPACE_H
#define INC_10_BEJEWELED_TYPES_NAMESPACE_H
#pragma once

#include <ctime>
#include <random>
#include <iostream>
#include <SFML/Graphics/Color.hpp>
#include "../Debug/debuggy.h"

namespace TYPES
{
    //srand(time(NULL));
    //int random = (rand(time(0)) % 11) ;
    enum COLOR{
        RED,
        VIOLET,
        BLUE,
        GREEN,
        YELLOW,
        ORANGE,
        RED_ORANGE,
        RED_VIOLET,
        BLUE_VIOLET,
        BLUE_GREEN,
        YELLOW_GREEN,
        YELLOW_ORANGE,
        CYAN,
        MAGENTA,
        BLACK,
        WHITE,
        TRANSPARENT,
    };

    enum ACTION{
        Standard,       // 0
        BOMB,       // 1
        LINE,
        SWIRL,
        NUKE,
    };


    enum Direction{
        up,
        down,
        left,
        right,
    };
}



static sf::Color setcolor(TYPES::COLOR input){
    switch (input){
        case TYPES::RED:
            debuggy("Color Selected: Red");
            return(sf::Color::Red);
        case TYPES::RED_VIOLET:
            debuggy("Color Selected: Red Violet");
            return(sf::Color(199,21,133,255));
        case TYPES::VIOLET:
            debuggy("Color Selected: Violet");
            return(sf::Color(148,0,211,255));
        case TYPES::BLUE_VIOLET:
            debuggy("Color Selected: Blue Violet");
            return(sf::Color(138,43,226,255));
        case TYPES::BLUE:
            debuggy("Color Selected: Blue");
            return(sf::Color::Blue);
        case TYPES::BLUE_GREEN:
            debuggy("Color Selected: Blue Green");
            return(sf::Color(13,152,186,255));
        case TYPES::GREEN:
            debuggy("Color Selected: Green");
            return(sf::Color::Green);
        case TYPES::YELLOW_GREEN:
            debuggy("Color Selected: Yellow Green");
            return(sf::Color(154,205,50));
        case TYPES::YELLOW:
            debuggy("Color Selected: Yellow");
            return(sf::Color::Yellow);
        case TYPES::YELLOW_ORANGE:
            debuggy("Color Selected: Yellow Orange");
            return(sf::Color(255,174,66));
        case TYPES::ORANGE:
            debuggy("Color Selected: Orange");
            return(sf::Color(255,165,0));
        case TYPES::RED_ORANGE:
            debuggy("Color Selected: Red Orange");
            return(sf::Color(231,48,0));
        case TYPES::CYAN:
            debuggy("Color Selected: Cyan");
            return(sf::Color::Cyan);
        case TYPES::MAGENTA:
            debuggy("Color Selected: Megenta");
            return(sf::Color::Magenta);
        case TYPES::BLACK:
            debuggy("Color Selected: Black");
            return(sf::Color::Black);
        case TYPES::WHITE:
            debuggy("Color Selected: White");
            return(sf::Color::White);
        case TYPES::TRANSPARENT:
            debuggy("Color Selected: Transparent");
            return(sf::Color::Transparent);
    }

}
static std::string operator+(std::string input, TYPES::COLOR Colour){
    switch (Colour){
        case TYPES::RED:
            debuggy("input + Red");
            return(input + "Red ");
        case TYPES::RED_VIOLET:
            debuggy("Color: Red Violet");
            return(input + "Red Violet ");
        case TYPES::VIOLET:
            debuggy("Color: Violet");
            return(input + "Violet ");
        case TYPES::BLUE_VIOLET:
            debuggy("Color: Blue Violet");
            return(input + "Blue Violet ");
        case TYPES::BLUE:
            debuggy("Color: Blue");
            return(input + "Blue ");
        case TYPES::BLUE_GREEN:
            debuggy("Color: Blue Green");
            return(input + "Blue Green ");
        case TYPES::GREEN:
            debuggy("Color: Green");
            return(input + "Green ");
        case TYPES::YELLOW_GREEN:
            debuggy("Color: Yellow Green");
            return(input + "Yellow Green ");
        case TYPES::YELLOW:
            debuggy("Color: Yellow");
            return(input + "Yellow ");
        case TYPES::YELLOW_ORANGE:
            debuggy("Color: Yellow Orange");
            return(input + "Yellow Orange");
        case TYPES::ORANGE:
            debuggy("Color: Orange");
            return(input + "Orange ");
        case TYPES::RED_ORANGE:
            debuggy("Color: Red Orange");
            return(input + "Red Orange ");
        case TYPES::CYAN:
            debuggy("Color: Cyan");
            return(input + "Cyan ");
        case TYPES::MAGENTA:
            debuggy("Color: Megenta");
            return(input + "Magenta ");
        case TYPES::BLACK:
            debuggy("Color: Black");
            return(input + "Black ");
        case TYPES::WHITE:
            debuggy("Color: White");
            return(input + "White ");
        case TYPES::TRANSPARENT:
            debuggy("Color: Transparent");
            return(input + "Transparent ");
    }
}
//static std::ostream               DOOOO MEEEEE

static std::ostream& operator<<(std::ostream& os, TYPES::COLOR colour){
    switch (colour){
        case TYPES::RED:
            os << "RED ";
            break;
        case TYPES::RED_VIOLET:
            os << "RED-VIOLET ";
            break;
        case TYPES::VIOLET:
            os << "VIOLET ";
            break;
        case TYPES::BLUE_VIOLET:
            os << "BLUE-VIOLET ";
            break;
        case TYPES::BLUE:
            os << "BLUE ";
            break;
        case TYPES::BLUE_GREEN:
            os << "BLUE-GREEN ";
            break;
        case TYPES::GREEN:
            os << "GREEN ";
            break;
        case TYPES::YELLOW_GREEN:
            os << "YELLOW-GREEN ";
            break;
        case TYPES::YELLOW:
            os << "YELLOW ";
            break;
        case TYPES::YELLOW_ORANGE:
            os << "YELLOW-ORANGE ";
            break;
        case TYPES::ORANGE:
            os << "ORANGE ";
            break;
        case TYPES::RED_ORANGE:
            os << "RED-ORANGE ";
            break;
        case TYPES::CYAN:
            os << "CYAN ";
            break;
        case TYPES::MAGENTA:
            os << "MAGENTA ";
            break;
        case TYPES::BLACK:
            os << "BLACK ";
            break;
        case TYPES::WHITE:
            os << "WHITE ";
            break;
        case TYPES::TRANSPARENT:
            os << "TRANSPARENT ";
            break;
    }
    return os;
}
#endif //INC_10_BEJEWELED_TYPES_NAMESPACE_H
