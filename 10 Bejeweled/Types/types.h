//
// Created by ian on 2/7/19.
//

#ifndef INC_10_BEJEWELED_TYPES_H
#define INC_10_BEJEWELED_TYPES_H
#pragma once

#include "Types_Namespace.h"

class Types{
public:
    Types(){};
    virtual ~Types(){};
    virtual std::pair<TYPES::COLOR, bool> get_Color() const = 0;
    virtual TYPES::ACTION get_Action() = 0;
};

bool operator==(const Types& a, const Types& b);

#endif //INC_10_BEJEWELED_TYPES_H
