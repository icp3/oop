//
// Created by ian on 4/22/19.
//

#include "check.h"
#include <iostream>
#include <cstring>
#include "./Debug/debuggy.h"

std::string check(const std::string& arr, bool& easy, bool& medium, bool& hard) {
    std::cout << "Beginning of Check\nArr:" << arr << "\n";
    if (arr[0] != '-') {
        std::cout << "Return Filename: " << arr << "\n";
        return arr;
    } else if (arr == "--Easy")  {
        std::cout << "--Easy Selected.\n";
        easy = true;
        medium = false;
        hard = false;
        return "";
    } else if (arr == "--Medium")  {
        std::cout << "--Medium Selected.\n";
        easy = false;
        medium = true;
        hard = false;
        return "";
    } else if (arr == "--Hard")  {
        std::cout << "--Hard Selected.\n";
        easy = false;
        medium = false;
        hard = true;
        return "";
    } else if (arr == "--Debug")  {
        std::cout << "--Debug selected.\n";
        DEBUG::debug = true;
        return "";
    }

    std::cout <<"Middle:\narr size: " << arr.size() << "\n";
    int x = 1;
    while ( x != arr.size()) {
        switch (toupper(arr[x])) {
            case 'D':
                DEBUG::debug = true;
                std::cout << "Debug selected.\n";
                break;
            case 'E':

                easy = true;
                medium = false;
                hard = false;
                std::cout << "Easy selected.\n";
                break;
            case 'M':
                easy = false;
                medium = true;
                hard = false;
                std::cout << "Medium selected.\n";
                break;
            case 'H':
                easy = false;
                medium = false;
                hard = true;
                std::cout << "Hard selected.\n";
                break;
            default:
                std::cout << "Illegal Command -" << arr[x] << "\n";
        }
        ++x;
    }
    return "";
}