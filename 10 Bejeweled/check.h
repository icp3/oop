//
// Created by ian on 4/22/19.
//

#ifndef INC_10_BEJEWELED_CHECK_H
#define INC_10_BEJEWELED_CHECK_H
#include <string>
#pragma once

std::string check(const std::string& arr, bool& easy, bool& medium, bool& hard);



#endif //INC_10_BEJEWELED_CHECK_H
