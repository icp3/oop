//
// Created by ian on 5/2/19.
//

#include "gui.h"

bool GUI::click(const sf::Event &e) {
    float offset_x = width / grid.size();
    float offset_y = height / grid[0].size();
    static bool first_click = true;
    static std::pair<int,int> first_location, second_location;
    if(e.MouseButtonReleased) {
        if (first_click) {
            first_location.first = e.mouseButton.x / offset_x;
            first_location.second = e.mouseButton.y / offset_y;
            first_click = false;
        } else {
            second_location.first = e.mouseButton.x/offset_x;
            second_location.second = e.mouseButton.y/offset_y;
            first_click = true;
            if (find_match(first_location, second_location)) {
                debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CLICK + GUI_DEBUG::FIND_MATCH + "Found true");
                if(the_game->check_location(first_location,second_location)) {
                    update_grid_color();
                    return true;
                }
            }
        }
    } else {
        if (first_click) {
            first_location.first = e.touch.x/offset_x;
            first_location.second = e.touch.y/offset_y;
            first_click = false;
        } else {
            second_location.first = e.touch.x/offset_x;
            second_location.second = e.touch.y/offset_y;
            first_click = true;
            if (find_match(first_location, second_location)) {
                debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CLICK + GUI_DEBUG::FIND_MATCH + "Found true");
                if(the_game->check_location(first_location,second_location)) {
                    update_grid_color();
                    return true;
                }
            }
        }
    }
    return false;
}