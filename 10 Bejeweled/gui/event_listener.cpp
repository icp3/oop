//
// Created by ian on 4/23/19.
//
#include "gui.h"

bool GUI::event_listener(sf::Event& event) {
    switch (event.type) {
        case sf::Event::Closed:
            debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::EVENT_LISTENER + "Event: Closed");
            this->close();
            return false;
        case sf::Event::MouseButtonReleased:
            debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::EVENT_LISTENER + "Event: MouseButtonReleased");
            return click(event);
            break;
        case sf::Event::TouchEnded:
            debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::EVENT_LISTENER + "Event: TouchEnded");
            return click(event);
            break;
        default:
            break;
    }
}

