//
// Created by ian on 4/25/19.
//

#include "gui.h"

bool GUI::find_match(std::pair<int,int> first_click, std::pair<int,int> second_click) {
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::FIND_MATCH + "Started");
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::FIND_MATCH + "First Click X: " + std::to_string(first_click.first) + " Y: " + std::to_string(first_click.second));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::FIND_MATCH + "Second Click X: " + std::to_string(second_click.first) + " Y: " + std::to_string(second_click.second));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::FIND_MATCH + "Result: " + std::to_string(abs(int(first_click.first) - int(second_click.first))+ abs(int(first_click.second) - int(second_click.second))));

    return(abs(int(first_click.first) - int(second_click.first))+ abs(int(first_click.second) - int(second_click.second)) == 1);

}



