//
// Created by ian on 4/25/19.
//

#include "gui.h"


GUI::GUI(int width, int height) : grid(vector<vector<sf::CircleShape>>(height,vector<sf::CircleShape>(width,sf::CircleShape()))),
    Colors(vector<vector<sf::Color>>(height,vector<sf::Color>(width))),
    height(900), width(900)
{
    the_game = new Maze(width,height,6);

    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR + "Colors: size " + std::to_string(Colors.size()));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR + "Colors: size " + std::to_string(Colors[0].size()));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR + "grid: size " + std::to_string(grid.size()));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR + "grid: size " + std::to_string(grid[0].size()));

    debuggy( GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR + "Created Window");

    this->setFramerateLimit(60);
    debuggy( GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +  "Framerate set");


    debuggy( GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +  "Background Set");

    update_grid_color();
    debuggy( GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::CONSTRUCTOR +  "Updated Grid");

    for(int x = 0; x < grid.size(); ++x) {
        for (int y = 0; y < grid[x].size(); ++y) {
            grid[x][y].setPosition((900 / grid.size()) * x, 900 / grid.size() * y);
            grid[x][y].setRadius(900 / grid.size() / 2);
        }
    }
    create(sf::VideoMode(900, 900), "Bejeweled",sf::Style::Close);
    clear();

    the_game->check_everywhere();
}

GUI::~GUI() {
    for(auto x: grid)
        x.clear();
    grid.clear();
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::DESTRUCTOR + "Game Control Finished");
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::DESTRUCTOR + "the game has been Deleted");
}
