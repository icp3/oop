//
// Created by ian on 4/25/19.
//

#ifndef INC_10_BEJEWELED_GUI_H
#define INC_10_BEJEWELED_GUI_H
#pragma once

#include "../Boards/Maze.h"
#include <SFML/Graphics.hpp>

namespace GUI_DEBUG{
    static const std::string GUI_DEBUG = "Gui: ";

    static const std::string IS_OPEN = "is_open: ";
    static const std::string FIND_MATCH = "find_match: ";
    static const std::string LEGAL_CLICK = "legal_click: ";
    static const std::string UPDATE_GRID_COLOR = "update_grid_color: ";
    static const std::string RENDER_AGAIN = "render_again: ";

    static const std::string EVENT_LISTENER = "event_listener: ";
    static const std::string RESIZE = "resize: ";
    static const std::string DESTRUCTOR = "Destructor: ";
    static const std::string CONSTRUCTOR = "Constructor: ";
    static const std::string POLL_EVENT = "poll_event: ";

    static const std::string UPDATE_GRID_POSITION = "update_grid_position: ";
    static const std::string UPDATE_GRID_SIZE = "update_grid_position: ";
    static const std::string CLICK = "click: ";
}

using sf::Texture;
using sf::Vector2i;
using sf::Mouse;
using sf::Event;


class GUI : public sf::RenderWindow {

    float height, width;

    vector<vector<sf::Color>> Colors;

    Maze* the_game;

    vector<vector<sf::CircleShape>> grid;

    Maze maze;

    bool event_listener(sf::Event& event);

    bool find_match(std::pair<int,int> first_click, std::pair<int,int> second_click);

    void update_grid_color();

    bool click(const sf::Event& e);

    void render_again();

public:
    virtual ~GUI();

    GUI(int column, int row);

    void poll_event();
};


#endif //INC_10_BEJEWELED_GUI_H
