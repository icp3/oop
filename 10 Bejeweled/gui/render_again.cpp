//
// Created by ian on 4/25/19.
//

#include "gui.h"

void GUI::render_again() {
    clear();
    for(auto x: grid) {
        for (auto y: x) {
            draw(y);
        }
    }
    display();
}