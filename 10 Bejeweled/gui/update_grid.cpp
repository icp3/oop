//
// Created by ian on 4/25/19.
//

#include "gui.h"

void GUI::update_grid_color() {
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_COLOR + "Initilized");
    the_game->get_maze(Colors);
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_COLOR + "Colors Finished");

    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_COLOR + "Colors: size " + std::to_string(Colors.size()));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_COLOR + "Colors: size " + std::to_string(Colors[0].size()));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_COLOR + "grid: size " + std::to_string(grid.size()));
    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_COLOR + "grid: size " + std::to_string(grid[0].size()));

    for(int x = 0; x < Colors.size(); ++x)
        for(int y = 0; y < Colors[x].size(); ++y)
            grid[x][y].setFillColor(Colors[x][y]);
}

//void GUI::update_grid_position(const sf::Event& e) {
//    float temp_x = e.size.width / grid.size();
//    float temp_y = e.size.height / grid[0].size();
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "getSize().x: " + std::to_string(getSize().x));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "getSize().y: " + std::to_string(getSize().y));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "eventSize x: " + std::to_string(e.size.width));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "eventSize y: " + std::to_string(e.size.height));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "temp_x: " + std::to_string(temp_x));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "temp_y: " + std::to_string(temp_y));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "local bounds width: " + std::to_string(grid[0][0].getLocalBounds().width));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "local bounds height: " + std::to_string(grid[0][0].getLocalBounds().height));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "global bounds width: " + std::to_string(grid[0][0].getGlobalBounds().width));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "global bounds height: " + std::to_string(grid[0][0].getGlobalBounds().height));
//
//
//
//    for(int x = 0; x < grid.size(); ++x)
//        for (int y = 0; y < grid[x].size(); ++y) {
//            grid[x][y].setPosition(temp_x * x, temp_y * y);
//            debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_POSITION + "x: " + std::to_string(temp_x * x) + " y: " + std::to_string((temp_y * y)));
//        }
//}
//
//void GUI::update_grid_size(const sf::Event& e) {
//    sf::Vector2f scale(e.size.width / grid.size() / 2,e.size.height / grid.size() / 2);
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_SIZE + "scale.x: " + std::to_string(scale.x));
//    debuggy(GUI_DEBUG::GUI_DEBUG + GUI_DEBUG::UPDATE_GRID_SIZE + "scale.y: " + std::to_string(scale.y));
//
//
//    for(int x = 0; x < grid.size(); ++x){
//        for(int y = 0; y < grid[x].size(); ++y){
//            grid[x][y].setScale(scale);
//        }
//    }
//}