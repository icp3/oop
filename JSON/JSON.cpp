//
// Created by ian on 4/1/19.
//

#include "JSON.h"
#include "objectJSON.h"
#include "numberJSON.h"
#include "array.h"
#include "stringJson.h"
#include "booleanJSON.h"
#include <stack>


namespace JN{
    const string search = "--search";
    const string  number = "--number";
    const string inlin = "--inline";

    const std::vector<string> options= {search, number, inlin};

    vector<bool> settings(options.size(), false);

}

JSON::JSON() : filename(""){

}

JSON::JSON(const string &filename) : filename(filename){
    std::ifstream file(filename);
    if(!file.is_open())
        throw std::invalid_argument("File not found.\n\n");
    file.close();
}

void JSON::add_search(string term) {
    search.push_back(term);
}

void JSON::get_search_terms(vector<string>& ret) {
    ret.clear();
    for(auto i : search)
        ret.push_back(i);
}


void JSON::parse() {
    std::ifstream file(filename);
    if(!file.is_open())
        throw std::invalid_argument("File not found.\n\n");

    std::vector<organizedJSON*> sta;

    string one, two;
    bool escape = false, quote = false, first = true, secondItem = false,
        digit = false, boolean = false, stringbool = false, booleon_value = false;

    char c;
    file.get(c);

    int i = 0;

    while(!file.eof()){

        if(ispunct(c)){
            switch(c){
                case '{':
                    if(quote) {
                        if (first) {
                            one.push_back(c);
                        }
                        else {
                            two.push_back(c);
                        }
                    }
                    else {
                        organizedJSON* tmp = new objectJSON(sta.size(), one);
                        sta.push_back(tmp);
//                        order.push_back(tmp);
                        one.clear();
                        two.clear();
                        first = true;
                    }
                    //std::cout << "Creating Object: " << sta.back()->get_name() << " Level: "<< sta.back()->get_level() << "\n";
                    break;
                case '}':
                    if(quote){
                        if(first)
                            one.push_back(c);
                        else
                            two.push_back(c);
                    }
                    else {
                        if(secondItem && !one.empty()){
                            if(digit){
                                nullJSON* tmp = new numberJSON(sta.size(), one, i);
                                //std::cout << "Creating Digit: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";
                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                                digit = false;
                                i = 0;
                            }
                            else if(boolean){
                                nullJSON* tmp = new booleanJSON(sta.size(),one, booleon_value);
                                //std::cout << "Creating Boolean: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";

                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                                boolean = false;
                            }
                            else if(stringbool){
                                nullJSON* tmp = new stringJson(sta.size(),one, two);
                                //std::cout << "Creating String: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";

                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                                stringbool = false;
                            }
                            else{
                                nullJSON* tmp = new nullJSON(sta.size(), one);
                                //std::cout << "Creating Null: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";
                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                            }
                        }
                        //std::cout << "Finishing Object: " << sta.back()->get_name() << " Level: " << sta.back()->get_level() << "\n";
                        data.push_back(sta.back());
                        sta.pop_back();
                    }
                    break;
                case ',':
                    if(quote){
                        if(first)
                            one.push_back(c);
                        else
                            two.push_back(c);
                    }
                    else {
                        if (digit) {
                            nullJSON *tmp = new numberJSON(sta.size(), one, i);
                            //std::cout << "Creating Digit: " << tmp->get_name() << " Level: " << tmp->get_level()<< "\n";
                            sta.back()->emplace_back(tmp);
                            one.clear();
                            two.clear();
                            first = true;
                            digit = false;
                            i = 0;
                        } else if (boolean) {
                            nullJSON *tmp = new booleanJSON(sta.size(), one, booleon_value);
                            //std::cout << "Creating Boolean: " << tmp->get_name() << " Level: " << tmp->get_level()<< "\n";

                            sta.back()->emplace_back(tmp);
                            one.clear();
                            two.clear();
                            first = true;
                            boolean = false;
                        } else if (stringbool && two.size()) {
                            nullJSON *tmp = new stringJson(sta.size(), one, two);
                            //std::cout << "Creating String: " << tmp->get_name() << " Level: " << tmp->get_level()<< "\n";
                            sta.back()->emplace_back(tmp);
                            one.clear();
                            two.clear();
                            first = true;
                            stringbool = false;
                        } else if(one.size()) {
                            nullJSON* tmp = new nullJSON(sta.size(), one);
                            //std::cout << "Creating Null: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";
                            sta.back()->emplace_back(tmp);
                            one.clear();
                            two.clear();
                            first = true;
                        }

                    }
                    break;
                case '[':
                    if(quote){
                        if(first)
                            one.push_back(c);
                        else
                            two.push_back(c);
                    }
                    else{
                        organizedJSON* tmp = new arrayJSON(sta.size(), one);
                        //order.push_back(tmp);
                        sta.push_back(tmp);
                        one.clear();
                        two.clear();
                        first = true;
                        //std::cout << "Creating Array: " << sta.back()->get_name() << " Level: "<< sta.back()->get_level() << "\n";
                    }
                    break;
                case ']':
                    if(quote){
                        if(first)
                            one.push_back(c);
                        else
                            two.push_back(c);
                    }
                    else {
                        if(secondItem && one.size()){
                            if(digit){
                                nullJSON* tmp = new numberJSON(sta.size(), one, i);
                                //std::cout << "Creating Digit: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";
                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                                digit = false;
                                i = 0;
                            }
                            else if(boolean){
                                nullJSON* tmp = new booleanJSON(sta.size(),one, booleon_value);
                                //std::cout << "Creating Boolean: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";

                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                                boolean = false;
                            }
                            else if(stringbool && two.size()){
                                nullJSON* tmp = new stringJson(sta.size(),one, two);
                                //std::cout << "Creating String: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";
                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                                stringbool = false;
                            }
                            else{
                                nullJSON* tmp = new nullJSON(sta.size(), one);
                                //std::cout << "Creating Null: " << tmp->get_name() << " Level: " << tmp->get_level() << "\n";
                                sta.back()->emplace_back(tmp);
                                one.clear();
                                two.clear();
                                first = true;
                            }
                        }
                        data.push_back(sta.back());
                        sta.pop_back();
                    }
                    break;
                case '"':
                    {
                        if(escape&&quote){
                            if(first)
                                one.push_back(c);
                            else
                                two.push_back(c);
                            escape = false;
                        }
                    }
                    if (quote)
                        quote = false;
                    else {
                        if (secondItem) { stringbool = true; }
                        quote = true;
                    }

                    break;
                case '\\':
                        if(quote) {
                            escape = true;
                            if(first)
                                one.push_back(c);
                            else
                                two.push_back(c);
                        }
                    break;
                case ':':
                    if(quote){
                        if(first)
                            one.push_back(c);
                        else
                            two.push_back(c);
                    }
                    else {
                        secondItem = true;
                        first = false;
                    }
                    break;
                default:
                    if(quote){
                        if(first)
                            one.push_back(c);
                        else
                            two.push_back(c);
                    }
                    break;
            }
        }
        else if(isalpha(c)){
            if(quote){
                if(first)
                    one.push_back(c);
                else
                    two.push_back(c);
            }
            else{
                switch(c){
                    case 'n':
                        while(c != ' ' && c != ',')
                            file.get(c);
                        boolean = false;
                        digit = false;
                        stringbool = false;
                        secondItem = true;
                        break;
                    case 't':
                        while(c != ' ' && c != ',')
                            file.get(c);
                        boolean = true;
                        digit = false;
                        stringbool = false;
                        booleon_value = true;
                        secondItem = true;
                        break;
                    case 'f':
                        while(c != ' ' && c != ',')
                            file.get(c);
                        boolean = true;
                        digit = false;
                        stringbool = false;
                        booleon_value = false;
                        secondItem = true;
                        break;
                    default:
                        break;
                }
            }
        }
        else if(isdigit(c)){
            if(quote){
                if(first)
                    one.push_back(c);
                else
                    two.push_back(c);
            }
            else {
                secondItem = true;
                digit = true;
                i *= 10;
                i += (c - '0');
            }
        }
        else if(isblank(c)){
            if(quote){
                if(first)
                    one.push_back(c);
                else
                    two.push_back(c);
            }
        }
        file.get(c);
    }
}

void JSON::search_terms(std::ostream &os) {
    for(auto& x : search){
        for(auto y : data){
            y->search(x, os);
        }
    }
}

int JSON::get_objects() {
    int size = 0;
    for(auto i : data)
        size += i->objects();

    return size;
}