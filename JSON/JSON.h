//
// Created by ian on 4/1/19.
//

#ifndef JSON_JSON_H
#define JSON_JSON_H
#pragma once

#include "organizedJSON.h"
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

using std::vector;

namespace JN{
    extern const string search;
    extern const string number;
    extern const string inlin;

    extern const vector<string> options;

    extern vector<bool> settings;
}

using std::string;

class JSON {
    vector<organizedJSON*> data;
    vector<organizedJSON*> order;
    vector<string> search;

    string filename;
public:
    JSON();

    explicit JSON(const string& filenames);

    void add_search(string term);

    void get_search_terms(vector<string>& ret);

    void parse();

    void search_terms(std::ostream& os = std::cout);

    int get_objects();
};




#endif //JSON_JSON_H
