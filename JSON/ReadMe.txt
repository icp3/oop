 Args:
    --inline :
    -i : inline all ( Default Pretty print )
             * WARNING CANNOT BE USED WITH number *

    --search
    -s : search particular word

    --number
    -n : number of items per line ( Default Pretty Print)
             * WARNING CANNOT BE USED WITH INLINE *


Last argument is always file name.

    ex: ./JSON ex.json


Can use multiple arguements using a single '-'
      items just search in a respective manner.

    ex: ./JSON -is Author ex.json
    ex: ./JSON -sn Author 5 ex.json

If using "--" arguements, arguement preceding that
    option must be that the value for the argument
    if there is one.

    ex: ./JSON --search author ex.json