//
// Created by ian on 4/1/19.
//

#include "array.h"

#include "vectorOutput.h"

void arrayJSON::print(std::ostream &os) const {
    os << " [ " << data << " ]\n";
}

bool arrayJSON::emplace_back(nullJSON* input) {
    if(data.size() == 0){
        if(input->is_array())
            array = true;
        else if(input->is_boolean())
            boolean = true;
        else if(input->is_digit())
            digit = true;
        else if(input->is_null())
            null = true;
        else if(input->is_object())
            object = true;
        else if(input->is_string())
            stringbool = true;
        data.emplace_back(input);
        return true;
    }
    else{
        if(input->is_array() && array){
            data.emplace_back(input);
            return true;
        }
        else if(input->is_boolean() && boolean){
            data.emplace_back(input);
            return true;
        }
        else if(input->is_digit() && digit){
            data.emplace_back(input);
            return true;
        }
        else if(input->is_null() && null){
            data.emplace_back(input);
            return true;
        }
        else if(input->is_object() && object){
            data.emplace_back(input);
            return true;
        }
        else if(input->is_string() && stringbool){
            data.emplace_back(input);
            return true;
        }
        else
            return false;
    }
}

void arrayJSON::search(const string &str, std::ostream &os) {
    if(name.find(str) != string::npos)
        os << "Array: " << name << " Level: " << level << "\n";

    for(auto i : data){
        i->search(str,os);
    }
}

int arrayJSON::objects() {
    int size = 0;

    for(auto i: data)
        size += i->objects();

    return size + 1;
}