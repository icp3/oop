//
// Created by ian on 4/1/19.
//

#ifndef JSON_ARRAY_H
#define JSON_ARRAY_H

#include <vector>
#include "organizedJSON.h"


class arrayJSON : public organizedJSON {
    std::vector<nullJSON*> data;

    bool stringbool, digit, boolean, array, object, null;
public:
    arrayJSON(int Level, const string &Name)
        : organizedJSON(Level, Name), stringbool(false),
            digit(false), boolean(false) {};

    void print(std::ostream &os) const override;

    bool emplace_back(nullJSON* input) override;
    
    bool is_null() override {return false;};
    
    bool is_array() override{return true;};

    void search(const string& str, std::ostream &os) override;

    int objects() override;
};



#endif //JSON_ARRAY_H
