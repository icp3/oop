//
// Created by ian on 4/1/19.
//

#include "booleanJSON.h"

void booleanJSON::print(std::ostream &os) const {
    os << data;
}

void booleanJSON::search(const string &sea, std::ostream &os) {
    if(name.find(sea) != string::npos || (sea == "true" && data) || (sea == "false" && !data))
        os << "Bool: " << name << ", Level: " << level << "\n";
}

