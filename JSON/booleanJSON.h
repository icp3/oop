//
// Created by ian on 4/1/19.
//

#ifndef JSON_BOOLEANJSON_H
#define JSON_BOOLEANJSON_H
#pragma once

#include "nullJSON.h"

class booleanJSON  : public nullJSON {
    bool data;
public:
    booleanJSON(int Level, const string &Name, bool boolean)
        : nullJSON(Level,Name), data(boolean) {};

    void print(std::ostream &os) const override;

    bool is_null() override {return false;};

    bool is_boolean() override {return true;};

    void search(const string& sea, std::ostream& os = std::cout) override;
};


#endif //JSON_BOOLEANJSON_H

