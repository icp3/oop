
#include <vector>
#include <iostream>
#include "JSON.h"

/**
 * Args:
 *      --inline :
 *      -i : inline all ( Default Pretty print )
 *           * WARNING CANNOT BE USED WITH number *
 *
 *      --search
 *      -s : search particular word
 *
 *      --number
 *      -n : number of items per line ( Default Pretty Print)
 *           * WARNING CANNOT BE USED WITH INLINE *
 *
 *
 * Last argument is always file name.
 *
 *      ex: ./JSON ex.json
 *
 *
 * Can use multiple arguements using a single '-'
 *      items just search in a respective manner.
 *
 *      ex: ./JSON -is Author ex.json
 *      ex: ./JSON -sn Author 5 ex.json
 *
 * If using "--" arguements, arguement preceding that
 *      option must be that the value for the argument
 *      if there is one.

 *      ex: ./JSON --search author ex.json
 *
 * Command line Arguments:
 * @param argv
 * @param argc
 * @return
 */




int main(int argv, char** argc) {
    vector<string> arg_vector;
    for(int i = 0; i < argv; ++i)
        arg_vector.emplace_back(argc[i]);

    JSON json(arg_vector[arg_vector.size() -1]);

    for(auto i: arg_vector)
        std::cout << i << " ";

    std::cout << "\n";

    if(argv > 1){
        int argf = 1;
        int args = 1;
        while(argf < argv - 1 && args < argv -1 ) {
            if (arg_vector[argf][0] == '-') {
                if (arg_vector[argf][1] == '-') {
                    bool check = true;
                    for (int i = 0; i < JN::options.size(); ++i) {

                        if (argc[argf] == JN::options[i]) { ;
                            if (argc[argf] == JN::search) {
                                string temp = argc[argf + 1];
                                json.add_search(temp);
                                argf += 2;
                                check = false;
                            } else {
                                ++argf;
                                check = false;
                            }
                            JN::settings[i] = true;
                        }
                    }
                    if (check) {
                        std::cout << "Unknown option: " << argc[argf] << "\n"
                                  << "Please look at the ReadMe.txt or the initial section of main.cpp.\n"
                                  << "Thank you and have a good day.\n\n";
                        return 0;
                    }
                } else {
                    argf = args + 1;

                    for (auto i = arg_vector[args].begin(); i != arg_vector[args].end(); ++i) {
                        switch (*i) {
                            case '\0':
                                args = argf + 1;
                                argf = args;
                                break;
                            case 'i':
                                JN::settings[1] = true;
                                break;
                            case 'n':
                                JN::settings[0] = true;
                                break;
                            case 's':
                                JN::settings[0] = true;
                                json.add_search(arg_vector[argf]);
                                ++argf;
                                break;
                            case '-':
                                break;
                            default:
                                std::cout << "Unknown option: -" << *i << "\n"
                                          << "Please look at the ReadMe.txt or the initial section of main.cpp.\n"
                                          << "Thank you and have a good day.\n\n";
                                return 0;
                        }
                    }
                }

            }
            else{

            }

        }
    } else{
        std::cout << "Please look at the ReadMe.txt or the initial section of main.cpp.\n"
                  << "Thank you and have a good day.\n\n";
        return 0;
    }
    vector<string> searchterms;
    json.get_search_terms(searchterms);

    for(auto i: searchterms)
        std::cout << i << "\n";

    json.parse();

    std::cout << "Number of objects: " << json.get_objects() << "\n";

    json.search_terms();

    return 0;
}