//
// Created by ian on 4/1/19.
//

#include "nullJSON.h"


nullJSON::nullJSON(int level, const string &name): level(level), name(name){
    checks[0] = false;
    checks[1] = false;
}

std::ostream &operator<<(std::ostream &os,
                         const nullJSON &json) {
    /// If not inline, create tabs.
    if(!json.checks[0]) {
        for (int x = 0; x < json.level; ++x) {
            os << "\t";
        }
    }

    if(!array)
        os << "\"" << json.name << "\" :";

    json.print(os);

    return os;
}

void nullJSON::print(std::ostream &os) const{
    os << "\n";
}

void nullJSON::search(const string &sea, std::ostream &os) {
    if(name.find(sea) != string::npos)
        os << "Null: " << name << ", Level: " << level << "\n";
}





