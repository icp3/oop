//
// Created by ian on 4/1/19.
//

#ifndef JSON_NULLJSON_H
#define JSON_NULLJSON_H
#pragma once

#include <string>
#include <ostream>
#include "VectorNamespace.h"
#include <iostream>

using std::string;

class nullJSON {
protected:
    /// Level of the output.
    int level;

    /**
     * 0: inline
     * 1: cont
     */
    bool checks[2];

    string name;

public:
    nullJSON(int level = 0, const string &name = "");

//    nullJSON(const string& NAME, int Level, bool inl, bool cont);
    friend std::ostream &
    operator<<(std::ostream &os,
               const nullJSON &json);

    virtual void search(const string& sea, std::ostream& os = std::cout);

    virtual void print(std::ostream& os) const;

    virtual bool is_digit(){return false;};

    virtual bool is_null(){return true;};

    virtual bool is_string(){return false;};

    virtual bool is_boolean(){return false;};

    virtual bool is_array(){return false;};

    virtual bool is_object(){return false;};

    virtual int objects(){return 1;};

    virtual string get_name(){return name;};

    virtual int get_level(){return level;};
};




#endif //JSON_NULLJSON_H
