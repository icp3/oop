//
// Created by ian on 4/1/19.
//

#ifndef JSON_NUMBERJSON_H
#define JSON_NUMBERJSON_H
#pragma once

#include "nullJSON.h"

class numberJSON : public nullJSON {
    long double data;
public:
    numberJSON(int Level, const string &Name, long double number)
        : nullJSON(Level, Name), data(number) {};

    void print(std::ostream &os) const override;

    void search(const string& sea, std::ostream& os = std::cout) override;

    bool is_digit() override{return true;};

    bool is_null() override{return false;};
};




#endif //JSON_NUMBERJSON_H
