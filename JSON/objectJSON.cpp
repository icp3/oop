//
// Created by ian on 4/1/19.
//

#include "objectJSON.h"
#include "vectorOutput.h"


void objectJSON::print(std::ostream &os) const {
    os <<" { " << data << " }";
}

bool objectJSON::emplace_back(nullJSON* input) {
    data.emplace_back(input);
    return true;
}

void objectJSON::search(const string &sea, std::ostream &os) {
    if(name.find(sea) != string::npos)
        os << "Object: " << name << " Level: " << level << "\n";

    for(auto i : data){
        i->search(sea, os);
    }
}

int objectJSON::objects() {
    int size = 0;

    for(auto i: data)
        size += i->objects();

    return size + 1;
}