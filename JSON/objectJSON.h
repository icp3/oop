//
// Created by ian on 4/1/19.
//

#ifndef JSON_OBJECTJSON_H
#define JSON_OBJECTJSON_H
#pragma once

#include <vector>

#include "organizedJSON.h"


class objectJSON : public organizedJSON {
    std::vector<nullJSON*> data;

public:
    objectJSON(int Level,  const string &Name)
        : organizedJSON(Level,Name) {};

    void print(std::ostream &os) const override;

    bool emplace_back(nullJSON* input) override;

    bool is_null() override {return false;};

    bool is_object() override {return true;};

    void search(const string& sea, std::ostream& os = std::cout) override;

    int objects() override;
};




#endif //JSON_OBJECTJSON_H

