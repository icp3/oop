//
// Created by ian on 4/2/19.
//

#ifndef JSON_ORGANIZEDJSON_H
#define JSON_ORGANIZEDJSON_H


#include "nullJSON.h"
#include <vector>
#include <iostream>


class organizedJSON : public nullJSON {
protected:
    std::vector<nullJSON*> data;
public:
    organizedJSON(int Level, const string &Name)
        : nullJSON(Level, Name) {};

    virtual bool emplace_back(nullJSON* input) = 0;


};



#endif //JSON_ORGANIZEDJSON_H
