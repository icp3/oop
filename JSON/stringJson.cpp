//
// Created by ian on 4/1/19.
//

#include "stringJson.h"


void stringJson::print(std::ostream &os) const {
    os << data;
}


void stringJson::search(const string &sea, std::ostream &os) {
    if(name.find(sea) != string::npos || data.find(sea) != string::npos)
        os << "String: " << name << " Level: " << level << " Data: \"" << data << "\"\n";
}