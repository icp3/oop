//
// Created by ian on 4/1/19.
//

#ifndef JSON_STRINGJSON_H
#define JSON_STRINGJSON_H
#pragma once

#include <string>

#include "nullJSON.h"

using std::string;

class stringJson : public nullJSON {
    std::string data;
public:
    stringJson(int Level, const string &Name, string text)
        : nullJSON(Level, Name), data(std::move(text)) {};

    void print(std::ostream &os) const override;

    bool is_null() override {return false;};

    bool is_string() override {return true;};

    void search(const string& sea, std::ostream& os = std::cout) override;

};


#endif //JSON_STRINGJSON_H

