//
// Created by ian on 4/2/19.
//

#ifndef JSON_VECTOROUTPUT_H
#define JSON_VECTOROUTPUT_H
#pragma once

#include <iostream>
#include <vector>


template<typename t>
std::ostream& operator<<(std::ostream &os, std::vector<t> data);

template<typename t>
std::ostream &operator<<(std::ostream &os,
                         std::vector<t> data) {

    for( auto i = data.begin(); i != data.end(); ++i){
        os << *i;
        if(i + 1 != data.end())
            os << ", ";
    }
    return os;
}

#endif //JSON_VECTOROUTPUT_H
