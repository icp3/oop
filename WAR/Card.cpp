//
// Created by ian on 2/3/19.
//

#include "Card.h"

card::card()
        : data((static_cast<unsigned>(types::Lose))){};



card::card(const card &p) : data(p.data) {};



card::card(unsigned int value, unsigned int suit)
        : data ((static_cast<unsigned>(types::Standard)|(suit << 4)|(value << 12))){};



card::card(types::Value value, types::Suit suit)
        : data((static_cast<unsigned>(types::Standard)|
                (static_cast<unsigned>(suit) << 4) |
                (static_cast<unsigned>(value) << 12))){};



card::card(types::Color color)
        : data((static_cast<unsigned>(types::Joker) & 0xF)|
               (static_cast<unsigned>(color) << 4)) {};



card::card(bool lose)
        : data((static_cast<unsigned>(types::Lose))) {};



types::Value card::getvalue() const{
    return(static_cast<types::Value>(data >> 12));
}



types::Suit card::getSuit() const{
    return( static_cast<types::Suit>((data >> 4 ) & 0xFF));
};



types::TYPE card::getType() const {
    return(static_cast<types::TYPE>(data & 0xF));
}



types::Color card::getColor() const{
    return(static_cast<types::Color>(data >> 4));
}



bool card::operator==(const card &rhs) const {
    std::cout << *this << "==" << rhs << "\n";
    if((this->getType() == types::Joker) && (rhs.getType() == types::Joker)){
        std::cout << "True\n";
        return true;
    }
    if((this->getType() == types::Standard) && (rhs.getType() == types::Standard)) {
        if(this->getvalue() == rhs.getvalue())
            std::cout << "true\n";
        else
            std::cout << "false\n";
        return (this->getvalue() == rhs.getvalue());
    }
    else {
        return false;
    }
}



bool card::operator!=(const card &rhs) const {
    return(!(*this == rhs));
}

bool card::operator >(const card &rhs) const
{
    if(getType() == types::TYPE::Joker) {
        if(rhs.getType() != types::Joker)
            return true;
    }
    if(getType() == types::TYPE::Standard)
    {
        if(rhs.getType() == types::TYPE::Standard)
        {
            return( static_cast<unsigned>(getvalue()) > static_cast<unsigned>(rhs.getvalue()));
        }
    }
    return false;
};

bool card::operator <(const card &rhs) const
{
    if((getType()==types::TYPE::Standard ) && (rhs.getType()==types::TYPE::Standard))
        return (static_cast<unsigned>(getvalue()) < static_cast<unsigned>(rhs.getvalue()));
    else
        return (getType() > rhs.getType());

}

card card::operator+(const card &a) const
{
    return card(getvalue()+a.getvalue(), getSuit());
}

bool card::operator<=(const card &rhs) const {
    return !(rhs < *this);
}

bool card::operator>=(const card &rhs) const {
    return !(*this < rhs);
}

card card::operator=(const card &a){
    data = a.data;
    return(a);
}

std::ostream & operator << (std::ostream &out, const card &c)
{
    if(types::Standard == c.getType())
    {
        switch(c.getSuit())
        {
            case types::hearts:
                out << "Heart ";
                break;
            case types::diamonds:
                out << "Diamond ";
                break;
            case types::spades:
                out << "Spade ";
                break;
            case types::clubs:
                out << "Club ";
                break;
        }

        switch(c.getvalue()) {
            case types::two:
                out << "2";
                break;
            case types::three:
                out << "3";
                break;
            case types::four:
                out << "4";
                break;
            case types::five:
                out << "5";
                break;
            case types::six:
                out << "6";
                break;
            case types::seven:
                out << "7";
                break;
            case types::eight:
                out << "8";
                break;
            case types::nine:
                out << "9";
                break;
            case types::ten:
                out << "10";
                break;
            case types::jack:
                out << "Jack";
                break;
            case types::queen:
                out << "Queen";
                break;
            case types::king:
                out << "King";
                break;
            case types::ace:
                out << "Ace";
                break;
            case types::fourteen:
                out << "15";
                break;
            case types::fifteen:
                out << "16";
                break;
            case types::sixteen:
                out << "17";
                break;
            case types::seventeen:
                out << "18";
                break;
            case types::eighteen:
                out << "19";
                break;
            case types::ninteen:
                out << "20";
                break;
            case types::twenty:
                out << "21";
                break;
            case types::twentyone:
                out << "22";
                break;
            case types::twentytwo:
                out << "23";
                break;
            case types::twentythree:
                out << "24";
                break;
            case types::twentyfour:
                out << "25";
                break;
            case types::twentyfive:
                out << "26";
                break;
            case types::twentysix:
                out << "27";
                break;
            case types::twentyseven:
                out << "28";
                break;
            case types::twentyeight:
                out << "29";
                break;
        }
    }
    else{
        switch(c.getColor())
        {
            case types::Red:
                out << "Red";
                break;
            case types::Black:
                out << "Black";
                break;
        }
        out << " Joker";
    }

    return out;
}

