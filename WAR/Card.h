//
// Created by ian on 2/3/19.
//

#ifndef WAR_CARD_H
#define WAR_CARD_H

#include <ostream>
#include "Type.h"
#include <vector>

class card{
public:
    card();

    card(const card &p);

    card(unsigned int value, unsigned int ssuit);

    explicit card(types::Value value, types::Suit suit);

    card(types::Color color);

    explicit card(bool lose);

    types::Value getvalue() const;

    types::Suit getSuit() const;

    types::TYPE getType() const;

    types::Color getColor() const;

    bool operator<=(const card &rhs) const;

    bool operator>=(const card &rhs) const;

    bool operator==(const card &rhs) const;

    bool operator!=(const card &rhs) const;


    bool operator>(const card &rhs) const;

    bool operator<(const card &rhs) const;

    friend std::ostream & operator << (std::ostream &out, const card &c);

    card operator+(const card &a ) const;

    card operator=(const card &a);

private:
    int data;

};





#endif //WAR_CARD_H
