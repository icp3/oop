//
// Created by ian on 3/13/19.
//

#ifndef WAR_CARD2_H
#define WAR_CARD2_H
#pragma once
#include "Type.h"
#include <ostream>


class Card {
public:
    virtual types::Value getValue() const = 0;
    virtual types::TYPE getType() const{return types::Lose;};
    virtual types::Suit getSuit() const = 0;
    virtual types::Color getColor() const = 0;

};

std::ostream & operator<<(std::ostream& outs, const Card* a);

#endif //WAR_CARD2_H
