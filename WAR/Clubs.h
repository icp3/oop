//
// Created by ian on 3/13/19.
//

#ifndef WAR_CLUBS_H
#define WAR_CLUBS_H


#include "Card2.h"

class ClubsAce : public Card {
public:
    ClubsAce()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::king;
        else
            return types::ace;
    };
};

class ClubsTwo : public Card {
public:
    ClubsTwo()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::queen;
        else
            return types::two;
    };
};

class ClubsThree : public Card {
public:
    ClubsThree()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::jack;
        else
            return types::three;
    };
};

class ClubsFour : public Card {
public:
    ClubsFour()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ten;
        else
            return types::four;
    };
};

class ClubsFive : public Card {
public:
    ClubsFive()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::nine;
        else
            return types::five;
    };
};

class ClubsSix : public Card {
public:
    ClubsSix()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::eight;
        else
            return types::six;
    };
};

class ClubsSeven : public Card {
public:
    ClubsSeven()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::seven;
        else
            return types::seven;
    };
};

class ClubsEight : public Card {
public:
    ClubsEight()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::six;
        else
            return types::eight;
    };
};

class ClubsNine : public Card {
public:
    ClubsNine()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::five;
        else
            return types::nine;
    };
};

class ClubsTen : public Card {
public:
    ClubsTen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::four;
        else
            return types::ten;
    };
};

class ClubsJack : public Card {
public:
    ClubsJack()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::three;
        else
            return types::jack;
    };
};

class ClubsQueen : public Card {
public:
    ClubsQueen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::two;
        else
            return types::queen;
    };
};

class ClubsKing : public Card {
public:
    ClubsKing()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ace;
        else
            return types::king;
    };
};

class ClubsFourteen : public Card {
public:
    ClubsFourteen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fourteen;};
};

class ClubsFifteen : public Card {
public:
    ClubsFifteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fifteen;};
};

class ClubsSixteen : public Card {
public:
    ClubsSixteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::sixteen;};
};

class ClubsSeventeen : public Card {
public:
    ClubsSeventeen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::seventeen;};
};

class ClubsEighteen : public Card {
public:
    ClubsEighteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::eighteen;};
};

class ClubsNinteen : public Card {
public:
    ClubsNinteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::ninteen;};
};

class ClubsTwenty : public Card {
public:
    ClubsTwenty() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twenty;};
};

class ClubsTwentyOne : public Card {
public:
    ClubsTwentyOne() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyone;};
};

class ClubsTwentyTwo : public Card {
public:
    ClubsTwentyTwo() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentytwo;};
};

class ClubsTwentyThree : public Card {
public:
    ClubsTwentyThree() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentythree;};
};

class ClubsTwentyFour : public Card {
public:
    ClubsTwentyFour() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfour;};
};

class ClubsTwentyFive : public Card {
public:
    ClubsTwentyFive() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfive;};
};

class ClubsTwentySix : public Card {
public:
    ClubsTwentySix() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentysix;};
};

class ClubsTwentySeven : public Card {
public:
    ClubsTwentySeven() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyseven;};
};

class ClubsTwentyEight : public Card {
public:
    ClubsTwentyEight() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::clubs;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyeight;};
};

#endif //WAR_CLUBS_H
