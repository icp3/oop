//
// Created by ian on 2/3/19.
//

#include "Deck.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock
#include <algorithm>    // std::shuffle
#include <cstdlib>


deck::deck(int playerNumba): numberOfPlayers(playerNumba)
{
    //std::cout << "deck::deck()\n";
    for(unsigned int x = 1; x <= 13; ++x) {
        for (unsigned int y = 0; y <= 3; ++y) {
            card temp(x, y);
            DECK.emplace_back(temp);
            //std::cout << "Creating card " << x << " " << y << " " << temp << "\n";
        }
    }
    for(unsigned int x = types::Red; x <= types::Black; ++x)
    {
        card temp(static_cast<types::Color>(x));
        //std::cout << "Creating card " << x << " " << temp << "\n";
        DECK.emplace_back(temp);
    }

    std::cout << "Number of players: " << numberOfPlayers << "\n";
    for(int x = 0; x < numberOfPlayers; ++x)
    {
        players.emplace_back(player());
    }
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    //std::shuffle(DECK.begin(), DECK.end(), std::default_random_engine(seed));

    std::cout << *this;

    //std::cout << "deck::divi();\n";
    for(int x = 0;!DECK.empty(); ++x)
    {
        //std:: cout << "Deck Size: " << DECK.size() << "\n";
//        std:: cout << "x = " << x << "\n";
        players[x % numberOfPlayers].emplace_back(DECK[DECK.size()-1]);
        DECK.pop_back();
    }
};


int deck::clash() {
    std::cout << "deck::clash()\n";
    card play[numberOfPlayers];
    bool autowar = false;
    for(int i = 0; i < numberOfPlayers; ++i)
    {
        if(!players[i].isEmpty()) {
            std::cout << "Player" << i + 1 << ":\n";

            if (check_answer("Top or Bottom?", 'T', 'B')) {
                play[i] = players[i].top();
            } else {
                play[i] = players[i].bottom();
            }

            std::cout << play[i] << "\n";

            DECK.emplace_back(play[i]);
            if (settings::GAME_SET[settings::Scouts] && (play[i] < card(7, 0))) {
                play[i] = scouts(play[i], i);
            }
            if (settings::GAME_SET[settings::AutomaticWarTwo] && (play[i] == card(2, 0)))
                autowar = true;
            if (settings::GAME_SET[settings::AutomaticWarJoker] && (play[i] == card(types::Red)))
                autowar = true;

        }
        else{
            play[i] = card();
        }
    }
    if(settings::GAME_SET[settings::WarIRL])
        warirl(DECK.begin(),DECK.end());
    return check_war(play,autowar);

};

bool deck::contin(int x)
{
    if(settings::GAME_SET[settings::FiveStraightBattles])
    {
        if(x == lastWinner)
            ++winningStreakint;
        else {
            winningStreakint = 1;
            lastWinner = x;
        }
        if(winningStreakint > 4)

            return false;
    }
    redistribute(x);
    //std::cout << "deck::contin()\n";
    int y = 0;
    for(int x = 0; x < numberOfPlayers; ++x)
    {
        if(players[x].isEmpty()) {
            findPlace(x);
        }
    }
    if(x == -1)
        return false;
    return true;
};

bool deck::threebeatsface(card x, card y) {
    return((settings::GAME_SET[settings::ThreesBeatFaces]) &&
           x.getvalue() == types::three &&
           (y.getvalue() == types::jack ||
           y.getvalue() == types::queen ||
           y.getvalue() == types::king));
};

bool deck::fourbeatsace(card x, card y) {
    return(settings::GAME_SET[settings::FoursBeatAces] &&
           x.getvalue() == types::four &&
           y.getvalue() == types::ace);
};



int deck::redistribute(int x) {
    std::cout << "Player " << x << " has won, please redistribute you earnings.";
    while(!DECK.empty())
    {
        std::cout << this;
    }
    return x;
}

std::ostream& operator<<(std::ostream &os, const deck &deck1) {
    int y = 0;
    for(auto x = deck1.DECK.begin(); x!= deck1.DECK.end(); ++x) {
        os << y+1 << " = " << *x << " ";
        if(y%5 == 0)
        {
            os << "\n";
        }
        ++y;
    }
    return os;
}

bool check_answer(std::string str, char a, char b)
{

    std::string temp;
    do {
        std::cout << str << " (" << a << "/ " << b << " )\n";
        std::getline(std::cin, temp);
        if(temp[0] == toupper(a))
            return true;
        if(temp[0] == toupper(b))
            return false;
        std::cin.ignore();
        std::cin.clear();

    } while(true);
}

card deck::scouts(card a, int i) {
    if (check_answer("Scouts: Would you like to add another card?", 'Y', 'N')) {
        card temp2;
        if (check_answer("Underdog: Top or Bottom?", 'T', 'B'))
            temp2 = players[i].top();
        else
            temp2 = players[i].bottom();
        DECK.emplace_back(temp2);
        std::cout << temp2 << "\n";
        a = a + temp2;
        std::cout << "Underdog combined: " << a << "\n";

    }
    return a;
}

int deck::check_war(card *play, bool autowar)
{
    for(int i = 0; i < numberOfPlayers; ++i)
    {
        std::cout << play[i] << " ";
    }
    std::cout << "\n";
    // Create checker
    bool check[numberOfPlayers];


    if(autowar) // If Autowar is enabled, all players go to war.
    {
        for(int i = 0; i<numberOfPlayers ; ++i)
            check[i] = true;
        std::cout << "\nOutcome: Autowar\n";
        return war(check);
    }

    else {

        card highest(true);


        for (int x = 0; x < numberOfPlayers; ++x) {
            if (settings::GAME_SET[settings::Peace]) {
                if (play[x] < highest) {
                    highest = play[x];
                }
            } else {
                if (play[x] > highest) {
                    highest = play[x];
                }
            }
        }

        if (settings::GAME_SET[settings::ThreesBeatFaces] && highest > card(10, 0)){
            highest = threeBattle(play, highest);
        }


        int count = 0;


        for (int x = 0; x < numberOfPlayers; ++x) {
            if (play[x] == highest)
                ++count;
        }

        if (count == 1) {
            for (int i = 0; i < numberOfPlayers; ++i)
                if (highest == play[i])
                    return i;
        }

        if (count == 0) {
            std::cout << "Outcome: Tie\n";
            return -1;
        }


        for(int i = 0; i < numberOfPlayers; ++i){
            if(highest == play[i])
                check[i] = true;
            else
                check[i] = false;
        }
        std::cout << "Outcome: War\n";
        return war(check);
    }
}



void deck::warirl(std::vector<card>::iterator begin, std::vector<card>::iterator end) {
    std::sort(begin, end);


    card highest(DECK.back());
    long temp = 0;

    for(; end != begin; --end)
    {
        if(highest == *end)
            ++temp;
    }
    --temp;

    if(temp == 0)
        return;

    while(temp > -1)
    {
        DECK.pop_back();
        --temp;
    }
}


int deck::war(bool *check) {
    card play[numberOfPlayers];
    bool autowar = false;
    int CardNumber = numberOfPlayers;
    int battles = 3;
    for (int i = 0; i < numberOfPlayers; ++i) {
        if (check[i])
        {
            if (settings::GAME_SET[settings::Peace])
                battles = 5;
            std::cout << "Player" << i + 1 << ":\n";
            while (battles > 0) {
                if (!players[i].isEmpty()) {
                    if (check_answer("Top or Bottom?", 'T', 'B'))
                        play[i] = players[i].top();
                    else
                        play[i] = players[i].bottom();
                    DECK.emplace_back(play[i]);
                    if (settings::GAME_SET[settings::Scouts] && (!players[i].isEmpty()) && (play[i] < card(7, 0))) {
                        card temp = play[i];
                        play[i] = scouts(play[i], i);
                        if(temp == play[i])
                            ++CardNumber;
                    }
                    if (settings::GAME_SET[settings::AutomaticWarTwo] && (play[i] == card(2, 0)))
                        autowar = true;
                    if (settings::GAME_SET[settings::AutomaticWarJoker] && (play[i] == card(types::Red)))
                        autowar = true;
                } else {
                    play[i] = card(true);
                }
                --battles;
            }
            battles = 3;
        }
    }
    if(settings::GAME_SET[settings::WarIRL])
        warirl(DECK.end()-CardNumber,DECK.end());
    return check_war(play,autowar);
}

bool deck::findPlace(int x)
{
    for(auto i = places.begin(); i != places.end(); ++i)
    {
        for(auto y = i->begin(); y != i->end(); ++y)
        {
            if(*y == x)
                return true;
        }
    }
    return false;
}

card deck::threeBattle(card *play, card highest)
{
    for(int i = 0; i < numberOfPlayers; ++i)
    {
        if(play[i] == card(3,0))
            return card(3,0);
    }
    return highest;
}
