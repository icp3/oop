//
// Created by ian on 2/3/19.
//

#ifndef WAR_DECK_H
#define WAR_DECK_H


#include "Player.h"
#include <vector>
#include <ostream>

class deck {
public:

    deck(int playerNumber = 2);

    int clash();

    bool contin(int x);

    int statistics();

    friend std::ostream &operator<<(std::ostream &os, const deck &deck1);

private:

    bool threebeatsface( card x, card y);

    bool fourbeatsace(card x, card y);

    int redistribute(int x);

    card scouts(card a, int i);

    int war(bool *check);

    void warirl(std::vector<card>::iterator begin, std::vector<card>::iterator end);

    int check_war(card *play, bool autowar = false);

    bool findPlace(int x);

    card threeBattle(card *play, card highest);

    /// Variables
    int numberOfPlayers;
    std::vector<card> DECK;
    std::vector<player> players;

    std::vector<std::vector<int>> places;

    int lastWinner;
    unsigned int winningStreakint;
};

bool check_answer(std::string str, char a, char b);


#endif //WAR_DECK_H
