//
// Created by ian on 3/13/19.
//

#include "DeckCreator.h"

namespace DeckSetup{
    const int DeckInt = 2;

    std::string joker("joker");
    std::string euchre("Euchre");

    std::string setup[DeckInt] = {joker, euchre};
}


DeckCreator::DeckCreator() {
}

DeckCreator::DeckCreator(char **argc, int argv) {
    bool checks[DeckSetup::DeckInt];

    for(int x = 0; x < DeckSetup::DeckInt; ++x)
        checks[x] = false;


    for(int x = 0; x < argv; ++x)
        for(int y = 0; y < DeckSetup::DeckInt; ++y)
            if(DeckSetup::setup[y] == argc[x])
                checks[y] = true;
    if(!checks[1]) {
        Deck.emplace_back(new HeartsAce());
        Deck.emplace_back(new HeartsTwo());
        Deck.emplace_back(new HeartsThree());
        Deck.emplace_back(new HeartsFour());
        Deck.emplace_back(new HeartsFive());
        Deck.emplace_back(new HeartsSix());
        Deck.emplace_back(new DiamondsAce());
        Deck.emplace_back(new DiamondsTwo());
        Deck.emplace_back(new DiamondsThree());
        Deck.emplace_back(new DiamondsFour());
        Deck.emplace_back(new DiamondsFive());
        Deck.emplace_back(new DiamondsSix());
        Deck.emplace_back(new ClubsAce());
        Deck.emplace_back(new ClubsTwo());
        Deck.emplace_back(new ClubsThree());
        Deck.emplace_back(new ClubsFour());
        Deck.emplace_back(new ClubsFive());
        Deck.emplace_back(new ClubsSix());
        Deck.emplace_back(new SpadesAce());
        Deck.emplace_back(new SpadesTwo());
        Deck.emplace_back(new SpadesThree());
        Deck.emplace_back(new SpadesFour());
        Deck.emplace_back(new SpadesFive());
        Deck.emplace_back(new SpadesSix());
    }
    Deck.emplace_back(new HeartsSeven());
    Deck.emplace_back(new HeartsEight());
    Deck.emplace_back(new HeartsNine());
    Deck.emplace_back(new HeartsTen());
    Deck.emplace_back(new HeartsJack());
    Deck.emplace_back(new HeartsQueen());
    Deck.emplace_back(new HeartsKing());

    Deck.emplace_back(new DiamondsSeven());
    Deck.emplace_back(new DiamondsEight());
    Deck.emplace_back(new DiamondsNine());
    Deck.emplace_back(new DiamondsTen());
    Deck.emplace_back(new DiamondsJack());
    Deck.emplace_back(new DiamondsQueen());
    Deck.emplace_back(new DiamondsKing());

    Deck.emplace_back(new ClubsSeven());
    Deck.emplace_back(new ClubsEight());
    Deck.emplace_back(new ClubsNine());
    Deck.emplace_back(new ClubsTen());
    Deck.emplace_back(new ClubsJack());
    Deck.emplace_back(new ClubsQueen());
    Deck.emplace_back(new ClubsKing());

    Deck.emplace_back(new SpadesSeven());
    Deck.emplace_back(new SpadesEight());
    Deck.emplace_back(new SpadesNine());
    Deck.emplace_back(new SpadesTen());
    Deck.emplace_back(new SpadesJack());
    Deck.emplace_back(new SpadesQueen());
    Deck.emplace_back(new SpadesKing());
    if(checks[0]) {
        Deck.emplace_back(new JokerRed());
        Deck.emplace_back(new JokerBlack());
    }
}


DeckCreator::~DeckCreator() {

}

std::ostream &operator<<(std::ostream &os,
                         const DeckCreator &creator) {
    for(int i = 0; i < creator.Deck.size(); ++i){
        os << creator.Deck[i] << " ";
        if(!(i % 5) && (i!=0))
            os << "\n";
    }
    return os;
}

std::ostream & operator<<(std::ostream& outs, const Card* a){
    switch(a->getType()) {
        case types::Joker:
            outs << "J";
            switch(a->getColor()){
                case types::Red:
                    outs << "R";
                    break;
                case types::Black:
                    outs << "B";
                    break;
            }
            return outs;
        case types::Standard:
            break;
        case types::Lose:
            outs << "Not a Card";
            return outs;
    }

    switch (a->getSuit()){
        case types::hearts:
            outs << "H";
            break;
        case types::diamonds:
            outs << "D";
            break;
        case types::spades:
            outs << "S";
            break;
        case types::clubs:
            outs << "C";
            break;
    }

    switch (a->getValue()){
        case types::ace:
            outs << "A";
            break;
        case types::two:
            outs << "2";
            break;
        case types::three:
            outs << "3";
            break;
        case types::four:
            outs << "4";
            break;
        case types::five:
            outs << "5";
            break;
        case types::six:
            outs << "6";
            break;
        case types::seven:
            outs << "7";
            break;
        case types::eight:
            outs << "8";
            break;
        case types::nine:
            outs << "9";
            break;
        case types::ten:
            outs << "T";
            break;
        case types::jack:
            outs << "J";
            break;
        case types::queen:
            outs << "Q";
            break;
        case types::king:
            outs << "K";
            break;
        case types::fourteen:
            outs << "14";
            break;
        case types::fifteen:
            outs << "15";
            break;
        case types::sixteen:
            outs << "16";
            break;
        case types::seventeen:
            outs << "17";
            break;
        case types::eighteen:
            outs << "18";
            break;
        case types::ninteen:
            outs << "19";
            break;
        case types::twenty:
            outs << "20";
            break;
        case types::twentyone:
            outs << "21";
            break;
        case types::twentytwo:
            outs << "22";
            break;
        case types::twentythree:
            outs << "23";
            break;
        case types::twentyfour:
            outs << "24";
            break;
        case types::twentyfive:
            outs << "25";
            break;
        case types::twentysix:
            outs << "26";
            break;
        case types::twentyseven:
            outs << "27";
            break;
        case types::twentyeight:
            outs << "28";
            break;
    }
    return outs;
}

