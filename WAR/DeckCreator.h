//
// Created by ian on 3/13/19.
//

#ifndef WAR_DECKCREATOR_H
#define WAR_DECKCREATOR_H
#pragma once

#include <vector>
#include <string>
#include <ostream>
#include "hearts.h"
#include "Spades.h"
#include "Diamonds.h"
#include "Clubs.h"
#include "Joker.h"


namespace DeckSetup{

    extern std::string joker;
    extern std::string euchre;

    extern std::string setup[2];
}


class DeckCreator {
    std::vector<Card*> Deck;
public:
    DeckCreator();
    DeckCreator(char **argc, int argv);

    friend std::ostream &
    operator<<(std::ostream &os,
               const DeckCreator &creator);

    virtual ~DeckCreator();

};

std::ostream & operator<<(std::ostream& outs, const Card* a);




#endif //WAR_DECKCREATOR_H
