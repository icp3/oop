//
// Created by ian on 3/13/19.
//

#ifndef WAR_DIAMONDS_H
#define WAR_DIAMONDS_H


#include "Card2.h"

class DiamondsAce : public Card {
public:
    DiamondsAce()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::king;
        else
            return types::ace;
    };
};

class DiamondsTwo : public Card {
public:
    DiamondsTwo()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::queen;
        else
            return types::two;
    };
};

class DiamondsThree : public Card {
public:
    DiamondsThree()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::jack;
        else
            return types::three;
    };
};

class DiamondsFour : public Card {
public:
    DiamondsFour()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ten;
        else
            return types::four;
    };
};

class DiamondsFive : public Card {
public:
    DiamondsFive()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::nine;
        else
            return types::five;
    };
};

class DiamondsSix : public Card {
public:
    DiamondsSix()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::eight;
        else
            return types::six;
    };
};

class DiamondsSeven : public Card {
public:
    DiamondsSeven()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::seven;
        else
            return types::seven;
    };
};

class DiamondsEight : public Card {
public:
    DiamondsEight()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::six;
        else
            return types::eight;
    };
};

class DiamondsNine : public Card {
public:
    DiamondsNine()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::five;
        else
            return types::nine;
    };
};

class DiamondsTen : public Card {
public:
    DiamondsTen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::four;
        else
            return types::ten;
    };
};

class DiamondsJack : public Card {
public:
    DiamondsJack()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::three;
        else
            return types::jack;
    };
};

class DiamondsQueen : public Card {
public:
    DiamondsQueen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::two;
        else
            return types::queen;
    };
};

class DiamondsKing : public Card {
public:
    DiamondsKing()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ace;
        else
            return types::king;
    };
};

class DiamondsFourteen : public Card {
public:
    DiamondsFourteen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fourteen;};
};

class DiamondsFifteen : public Card {
public:
    DiamondsFifteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fifteen;};
};

class DiamondsSixteen : public Card {
public:
    DiamondsSixteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::sixteen;};
};

class DiamondsSeventeen : public Card {
public:
    DiamondsSeventeen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::seventeen;};
};

class DiamondsEighteen : public Card {
public:
    DiamondsEighteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::eighteen;};
};

class DiamondsNinteen : public Card {
public:
    DiamondsNinteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::ninteen;};
};

class DiamondsTwenty : public Card {
public:
    DiamondsTwenty() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twenty;};
};

class DiamondsTwentyOne : public Card {
public:
    DiamondsTwentyOne() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyone;};
};

class DiamondsTwentyTwo : public Card {
public:
    DiamondsTwentyTwo() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentytwo;};
};

class DiamondsTwentyThree : public Card {
public:
    DiamondsTwentyThree() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentythree;};
};

class DiamondsTwentyFour : public Card {
public:
    DiamondsTwentyFour() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfour;};
};

class DiamondsTwentyFive : public Card {
public:
    DiamondsTwentyFive() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfive;};
};

class DiamondsTwentySix : public Card {
public:
    DiamondsTwentySix() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentysix;};
};

class DiamondsTwentySeven : public Card {
public:
    DiamondsTwentySeven() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyseven;};
};

class DiamondsTwentyEight : public Card {
public:
    DiamondsTwentyEight() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::diamonds;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyeight;};
};

#endif //WAR_DIAMONDS_H
