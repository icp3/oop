//
// Created by ian on 2/3/19.
//

#include "Help.h"

void help()
{
    std::cout << "       War\n\n"
         << "Arguements: \n\n"
         << "War\n"
         << "                               Two player standard game\n"
         << "War Options\n"
         << "                               Two Player Non standard game\n"
         << "War NumberOfPlayers\n"
         << "                               Multiplayer standard game\n"
         << "War NumberOfPlayers Options\n"
         << "                               Multiplayer non standard game\n\n"
         << "Options:\n\n"
         << "   AddOn\n"
         << "           Players may flip additional cards each war, but bust if going over 15\n"
         << "            (face cards are valued as 10).\n\n"
         << "   Scouts\n"
         << "           If the card you play is valued 5 or less, you have the option to play\n"
         << "           one additional card (a helper card). The combined value of the first \n"
         << "           card played and the helper card is the value used in the battle. \n"
         << "           (J = 11, Q = 12, K = 13, A = 14). One exception is that a helper card\n"
         << "           cannot be played If both players play the same card that is valued 5\n"
         << "           or less (ex: each player plays a 3). A maximum of one helper card can\n"
         << "           be played for each card played (for example, if a 4 is played and \n"
         << "           then a 3 is played as a helper for the 4, no more helper cards can be\n"
         << "           played to help the 4). If the card and helper card's combined value \n"
         << "           loses the battle, then both cards are lost to the other player.\n\n"
         << "   AutomaticWarJoker,\n"
         << "   AutomaticWarTwo,\n"
         << "           A certain card, typically a 2 or a Joker causes an automatic war.\n\n"
         << "   ThreesBeatFaces\n"
         << "           In this variation, a 3 wins against any face card, but still loses \n"
         << "           against other cards higher than it.\n\n"
         << "   FoursBeatAces\n"
         << "           Usually played alongside the above variation, here a 4 beats an ace,\n"
         << "           but loses against other cards higher than it.\n\n"
         << "   Underdog\n"
         << "           When a player has lost a war, he may check his three face down cards\n"
         << "           for a predetermined underdog card, usually 6 if playing with the \n"
         << "           above rules, and if one of the cards is a 6, he wins the war.\n\n"
         << "   Peace\n"
         << "           A simple variation played the opposite of War. Lowest Card wins.\n"
         << "           Instead of 3 cards being laid down in a peace (a war) 5 are, 1 for\n"
         << "           each letter in peace.\n\n"
         << "   WarIRL\n"
         << "           This version of War has a poetic similarity to war in real life; no\n"
         << "           one wins in war. If both players play a card of the same value \n"
         << "           (ranging from low to high, 2-Ace), both players remove that card from\n"
         << "            play. Coincidentally, this is also an easy version of the game for\n"
         << "           programmers to practice on since more than two cards will never be\n"
         << "           used at once.\n\n"
         << "   FiveStraightBattles\n"
         << "           If a player wins five straight battles, his opponent gives him his \n"
         << "           next faced down card.\n\n"
         << "   Help\n"
         << "           Outputs all options.\n\n";
}

void standard_game()
{
    std::cout << "standard_game()\n";
    deck bob;
    //do{
        bob.clash();
    //}
    //while(bob.contin());
}

void non_standard_game(int i)
{
    std::cout << "non_standard_game(int i)\n";
    std::cout<< "number of players: " << i << "\n\n";
    settings::output();
}