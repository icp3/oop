//
// Created by ian on 2/3/19.
//

#ifndef WAR_HELP_H
#define WAR_HELP_H

#include "Deck.h"

void help();

void standard_game();

void non_standard_game(int i = 2);

#endif //WAR_HELP_H
