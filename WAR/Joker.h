//
// Created by ian on 3/13/19.
//

#ifndef WAR_JOKER_H
#define WAR_JOKER_H

#include "Card2.h"



class JokerRed : public Card{
public:
    JokerRed() = default;
    types::TYPE getType() const override{return types::Joker;};
    types::Color getColor() const override{return types::Red;};
    types::Value getValue() const override{return types::ace;};
    types::Suit getSuit() const override{return types::none;};

};

class JokerBlack : public Card{
public:
    JokerBlack() = default;
    types::TYPE getType() const override{return types::Joker;};
    types::Color getColor() const override{return types::Black;};
    types::Value getValue() const override{return types::ace;};
    types::Suit getSuit() const override{return types::none;};
};


#endif //WAR_JOKER_H
