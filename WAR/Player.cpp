//
// Created by ian on 2/3/19.
//

#include "Player.h"

#include <algorithm>    // std::shuffle
#include <vector>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

player::player()
{
    //std::cout << "player::player();\n";
};


bool player::emplace_back(int x, int y)
{
    std::cout << "player::push(int x, int y)\n";
    hand.emplace_back(card(x,y));
}

bool player::emplace_back(card x)
{
    //std::cout << "player::push(" << x << ")\n";

    hand.emplace_back(x);

}

card player::top(){
    std::cout << "player::top()\n";
    card x = *(hand.begin());
    hand.pop_front();
    return x;
};

card player::bottom(){
    std::cout << "player::bottom()\n";
    card x = *(hand.end());
    hand.pop_back();
    return x;
}


bool player::isEmpty()
{
    return(hand.empty());
}

void player::moveCard(player x)
{
    std::cout << "player::moveCard(player x)\n";
    x.hand.splice(x.hand.end(), hand, hand.begin() );

}

std::ostream &operator<<(std::ostream &os, const player &player1) {
    os << "Game on engaged, no peeking.";
    return os;
}
