//
// Created by ian on 2/3/19.
//

#ifndef WAR_PLAYER_H
#define WAR_PLAYER_H


#include <vector>
#include "Card.h"
#include <list>
#include <ostream>

class player
{
public:
    player();

    bool emplace_back(int x, int y);

    bool emplace_back(card x);

    card top();

    card bottom();

    friend std::ostream &operator<<(std::ostream &os, const player &player1);

    bool isEmpty();

    void moveCard(player x);

private:
    std::list<card> hand;
};


#endif //WAR_PLAYER_H
