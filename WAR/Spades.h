//
// Created by ian on 3/13/19.
//

#ifndef WAR_SPADES_H
#define WAR_SPADES_H

#include "Card2.h"

class SpadesAce : public Card {
public:
    SpadesAce()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::king;
        else
            return types::ace;
    };
};

class SpadesTwo : public Card {
public:
    SpadesTwo()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::queen;
        else
            return types::two;
    };
};

class SpadesThree : public Card {
public:
    SpadesThree()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::jack;
        else
            return types::three;
    };
};

class SpadesFour : public Card {
public:
    SpadesFour()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ten;
        else
            return types::four;
    };
};

class SpadesFive : public Card {
public:
    SpadesFive()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::nine;
        else
            return types::five;
    };
};

class SpadesSix : public Card {
public:
    SpadesSix()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::eight;
        else
            return types::six;
    };
};

class SpadesSeven : public Card {
public:
    SpadesSeven()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::seven;
        else
            return types::seven;
    };
};

class SpadesEight : public Card {
public:
    SpadesEight()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::six;
        else
            return types::eight;
    };
};

class SpadesNine : public Card {
public:
    SpadesNine()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::five;
        else
            return types::nine;
    };
};

class SpadesTen : public Card {
public:
    SpadesTen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::four;
        else
            return types::ten;
    };
};

class SpadesJack : public Card {
public:
    SpadesJack()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::three;
        else
            return types::jack;
    };
};

class SpadesQueen : public Card {
public:
    SpadesQueen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::two;
        else
            return types::queen;
    };
};

class SpadesKing : public Card {
public:
    SpadesKing()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ace;
        else
            return types::king;
    };
};

class SpadesFourteen : public Card {
public:
    SpadesFourteen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fourteen;};
};

class SpadesFifteen : public Card {
public:
    SpadesFifteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fifteen;};
};

class SpadesSixteen : public Card {
public:
    SpadesSixteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::sixteen;};
};

class SpadesSeventeen : public Card {
public:
    SpadesSeventeen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::seventeen;};
};

class SpadesEighteen : public Card {
public:
    SpadesEighteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::eighteen;};
};

class SpadesNinteen : public Card {
public:
    SpadesNinteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::ninteen;};
};

class SpadesTwenty : public Card {
public:
    SpadesTwenty() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twenty;};
};

class SpadesTwentyOne : public Card {
public:
    SpadesTwentyOne() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyone;};
};

class SpadesTwentyTwo : public Card {
public:
    SpadesTwentyTwo() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentytwo;};
};

class SpadesTwentyThree : public Card {
public:
    SpadesTwentyThree() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentythree;};
};

class SpadesTwentyFour : public Card {
public:
    SpadesTwentyFour() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfour;};
};

class SpadesTwentyFive : public Card {
public:
    SpadesTwentyFive() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfive;};
};

class SpadesTwentySix : public Card {
public:
    SpadesTwentySix() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentysix;};
};

class SpadesTwentySeven : public Card {
public:
    SpadesTwentySeven() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyseven;};
};

class SpadesTwentyEight : public Card {
public:
    SpadesTwentyEight() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::spades;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyeight;};
};

#endif //WAR_SPADES_H
