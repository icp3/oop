//
// Created by ian on 2/3/19.
//
#include "Type.h"

namespace settings{
    bool GAME_SET[10] = {false,false,false,false,false,false,false,false,false,false};

    const std::string SETTING_NAME[] = {"Add On", "Scouts", "Automatic War Two","Automatic War Joker",
                                  "Threes Beat Faces","Fours Beat Aces",
                                  "Underdog", "Peace","WarIRL",
                                  "Five Straight Battles","Help"};
}

void settings::output()
{
    for(int x = 0; x < 11; ++x)
    {
        if(GAME_SET[x])
            std::cout << SETTING_NAME[x];
    }
}
