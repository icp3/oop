//
// Created by ian on 2/3/19.
//

#include <iostream>

#ifndef WAR_TYPE_H
#define WAR_TYPE_H
#pragma once

namespace types{

    enum TYPE{
        Joker = 0,
        Standard = 1,
        Lose = 2,
    };

    enum Color{
        Red = 0,
        Black = 1,
    };

    enum Suit{
        hearts = 0,
        diamonds = 1,
        spades = 2,
        clubs = 3,
        none,
    };

    enum Value{
        ace = 1,
        two = 2,
        three = 3,
        four = 4,
        five = 5,
        six = 6,
        seven = 7,
        eight = 8,
        nine = 9,
        ten = 10,
        jack = 11,
        queen = 12,
        king = 13,
        fourteen = 14,
        fifteen = 15,
        sixteen = 16,
        seventeen = 17,
        eighteen = 18,
        ninteen = 19,
        twenty = 20,
        twentyone = 21,
        twentytwo = 22,
        twentythree = 23,
        twentyfour = 24,
        twentyfive = 25,
        twentysix = 26,
        twentyseven = 27,
        twentyeight = 28,
    };
};

namespace settings{

    extern bool GAME_SET[10];

    extern const std::string SETTING_NAME[11];


    enum GAME{
        AddOn,               // Players may flip additional cards each war, but bust if going over 15 (face cards are valued as 10).
        Scouts,              // If the card you play is valued 5 or less, you have the option to play one additional card (a helper card). The combined value of the first card played and the helper card is the value used in the battle. (J = 11, Q = 12, K = 13, A = 14). One exception is that a helper card cannot be played If both players play the same card that is valued 5 or less (ex: each player plays a 3). A maximum of one helper card can be played for each card played (for example, if a 4 is played and then a 3 is played as a helper for the 4, no more helper cards can be played to help the 4). If the card and helper card's combined value loses the battle, then both cards are lost to the other player.
        AutomaticWarTwo,
        AutomaticWarJoker,
        ThreesBeatFaces,     // In this variation, a 3 wins against any face card, but still loses against other cards higher than it.
        FoursBeatAces,       // Usually played alongside the above variation, here a 4 beats an ace, but loses against other cards higher than it.
        Underdog,            // When a player has lost a war, he may check his three face down cards for a predetermined underdog card, usually 6 if playing with the above rules, and if one of the cards is a 6, he wins the war.
        Peace,               // A simple variation played the opposite of War. Lowest Card wins. Instead of 3 cards being laid down in a peace (a war) 5 are, 1 for each letter in peace.
        WarIRL,               // This version of War has a poetic similarity to war in real life; no one wins in war. If both players play a card of the same value (ranging from low to high, 2-Ace), both players remove that card from play. Coincidentally, this is also an easy version of the game for programmers to practice on since more than two cards will never be used at once.
        FiveStraightBattles, // If a player wins five straight battles, his opponent gives him his next faced down card.
    };

    void output();



};





#endif //WAR_TYPE_H
