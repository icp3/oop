//
// Created by ian on 3/13/19.
//

#ifndef WAR_HEARTS_H
#define WAR_HEARTS_H

#include "Card2.h"

class HeartsAce : public Card {
public:
    HeartsAce()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::king;
        else
            return types::ace;
    };
};

class HeartsTwo : public Card {
public:
    HeartsTwo()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::queen;
        else
            return types::two;
    };
};

class HeartsThree : public Card {
public:
    HeartsThree()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::jack;
        else
            return types::three;
    };
};

class HeartsFour : public Card {
public:
    HeartsFour()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ten;
        else
            return types::four;
    };
};

class HeartsFive : public Card {
public:
    HeartsFive()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::nine;
        else
            return types::five;
    };
};

class HeartsSix : public Card {
public:
    HeartsSix()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::eight;
        else
            return types::six;
    };
};

class HeartsSeven : public Card {
public:
    HeartsSeven()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::seven;
        else
            return types::seven;
    };
};

class HeartsEight : public Card {
public:
    HeartsEight()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::six;
        else
            return types::eight;
    };
};

class HeartsNine : public Card {
public:
    HeartsNine()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::five;
        else
            return types::nine;
    };
};

class HeartsTen : public Card {
public:
    HeartsTen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::four;
        else
            return types::ten;
    };
};

class HeartsJack : public Card {
public:
    HeartsJack()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::three;
        else
            return types::jack;
    };
};

class HeartsQueen : public Card {
public:
    HeartsQueen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::two;
        else
            return types::queen;
    };
};

class HeartsKing : public Card {
public:
    HeartsKing()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {
        if(settings::GAME_SET[settings::Peace])
            return types::ace;
        else
            return types::king;
    };
};

class HeartsFourteen : public Card {
public:
    HeartsFourteen()= default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fourteen;};
};

class HeartsFifteen : public Card {
public:
    HeartsFifteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::fifteen;};
};

class HeartsSixteen : public Card {
public:
    HeartsSixteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::sixteen;};
};

class HeartsSeventeen : public Card {
public:
    HeartsSeventeen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::seventeen;};
};

class HeartsEighteen : public Card {
public:
    HeartsEighteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::eighteen;};
};

class HeartsNinteen : public Card {
public:
    HeartsNinteen() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::ninteen;};
};

class HeartsTwenty : public Card {
public:
    HeartsTwenty() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twenty;};
};

class HeartsTwentyOne : public Card {
public:
    HeartsTwentyOne() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyone;};
};

class HeartsTwentyTwo : public Card {
public:
    HeartsTwentyTwo() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentytwo;};
};

class HeartsTwentyThree : public Card {
public:
    HeartsTwentyThree() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentythree;};
};

class HeartsTwentyFour : public Card {
public:
    HeartsTwentyFour() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfour;};
};

class HeartsTwentyFive : public Card {
public:
    HeartsTwentyFive() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyfive;};
};

class HeartsTwentySix : public Card {
public:
    HeartsTwentySix() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentysix;};
};

class HeartsTwentySeven : public Card {
public:
    HeartsTwentySeven() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyseven;};
};

class HeartsTwentyEight : public Card {
public:
    HeartsTwentyEight() = default;
    types::TYPE getType() const override  {return types::Standard;};
    types::Suit getSuit() const override  {return types::hearts;};
    types::Color getColor() const override  {return types::Red;};
    types::Value getValue() const override  {return types::twentyeight;};
};

#endif //WAR_HEARTS_H
