#include <iostream>
#include "Help.h"
#include <stdlib.h>
#include <cstring>

int main(int argc, char** argv)
{
    using namespace settings;
    int i = 0;
    if(argc == 1)
    {
        standard_game();
        return 0;
    }
    else if(argc > 1)
    {
        i = atoi(argv[1]);
        std::cout << "one\n";
        for(int y = 1; y < argc; ++y)
        {
            for(int x = 0; argv[y][x]; ++x)
            {
                argv[y][x]=tolower(argv[y][x]);
            }
            if(!strcmp("AddOn",argv[y]))
                GAME_SET[AddOn] = true;
            else if(!strcmp(argv[y], "Scounts"))
                GAME_SET[Scouts] = true;
            else if(!strcmp( argv[y], "AutomaticWarTwo"))
                GAME_SET[AutomaticWarTwo] = true;
            else if(!strcmp( argv[y], "ThreesBeatFaces"))
                GAME_SET[ThreesBeatFaces] = true;
            else if(!strcmp(argv[y], "FoursBeatAces"))
                GAME_SET[FoursBeatAces] = true;
            else if(!strcmp(argv[y], "Underdog"))
                GAME_SET[Underdog] = true;
            else if(!strcmp(argv[y], "Peace"))
                GAME_SET[Peace] = true;
            else if(!strcmp(argv[y], "WarIRL"))
                GAME_SET[WarIRL] = true;
            else if(!strcmp(argv[y], "FiveStraightBattles"))
                GAME_SET[FiveStraightBattles] = true;
            else if(!strcmp(argv[y], "Help")) {
                help();
                return 0;
            }
        }
    }

    if(i > 1)
    {
        non_standard_game(i);
        return 0;
    }
    else if(i == 1)
    {
        std::cout << "Must have more then 1 player.\n\n";
        return 0;
    }
    else {
        non_standard_game();
        return 0;
    }


}